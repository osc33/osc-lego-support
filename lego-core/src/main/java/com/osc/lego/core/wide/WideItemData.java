package com.osc.lego.core.wide;

/**
 * @author osc 2023/10/29.
 * 
 * 
 */
public interface WideItemData<
        T extends Enum<T> & WideItemType<T>,
        KEY> {
    T getItemType();

    KEY getKey();
}
