package com.osc.lego.core.query;

import org.springframework.stereotype.Indexed;

import java.lang.annotation.*;

/**
 * @author osc 2023/9/25.
 * 
 *
 *
 * 标记一个接口为 QueryService，系统自动生成实现的代理对象
 */
@Indexed
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Inherited
public @interface QueryServiceDefinition {

    Class domainClass();

    Class<? extends QueryRepository> repositoryClass();
}
