package com.osc.lego.core.excelasbean.support.write.row;

import lombok.Builder;
import lombok.Value;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;


@Value
@Builder
public class HSSFRowWriterContext {
    private final HSSFWorkbook workbook;
    private final HSSFSheet sheet;
}
