package com.osc.lego.core.excelasbean.support.reader.sheet;

/**
 *
 * 解析 class 信息，创建 HSSFSheetReader 对象
 */
public interface HSSFSheetReaderFactory {
    /**
     * 解析 Class 创建 HSSFSheetReader 对象
     * @param cls
     * @param <D>
     * @return
     */
    <D> HSSFSheetReader<D> createFor(Class<D> cls);
}
