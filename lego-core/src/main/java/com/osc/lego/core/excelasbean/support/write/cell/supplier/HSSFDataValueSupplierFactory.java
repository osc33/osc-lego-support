package com.osc.lego.core.excelasbean.support.write.cell.supplier;

import com.osc.lego.core.SmartComponent;

import java.lang.reflect.AnnotatedElement;


public interface HSSFDataValueSupplierFactory extends SmartComponent<AnnotatedElement> {
    HSSFValueSupplier create(AnnotatedElement annotatedElement);
}
