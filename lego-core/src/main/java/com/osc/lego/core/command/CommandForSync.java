package com.osc.lego.core.command;


public interface CommandForSync<KEY> extends Command{
    KEY getKey();
}
