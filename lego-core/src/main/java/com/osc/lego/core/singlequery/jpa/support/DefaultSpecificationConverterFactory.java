package com.osc.lego.core.singlequery.jpa.support;

import com.osc.lego.core.singlequery.jpa.SpecificationConverter;
import com.osc.lego.core.singlequery.jpa.SpecificationConverterFactory;
import com.osc.lego.core.singlequery.jpa.support.handler.JpaAnnotationHandler;

import java.util.List;

/**
 * @author osc 2023/8/30.
 * 
 * 
 */
public class DefaultSpecificationConverterFactory implements SpecificationConverterFactory {
    private final List<JpaAnnotationHandler> annotationHandlers;

    public DefaultSpecificationConverterFactory(List<JpaAnnotationHandler> annotationHandlers) {
        this.annotationHandlers = annotationHandlers;
    }

    @Override
    public <E> SpecificationConverter<E> createForEntity(Class<E> entityCls) {
        return new DefaultSpecificationConverter<>(entityCls, this.annotationHandlers);
    }
}
