package com.osc.lego.core.msg.sender.support;

import com.osc.lego.core.msg.sender.ReliableMessageCompensator;

import java.util.Date;

/**
 * @author osc 2023/10/16.
 * 
 *
 */
public class LocalTableBasedReliableMessageCompensator implements ReliableMessageCompensator {
    private final ReliableMessageSendService reliableMessageSendService;

    public LocalTableBasedReliableMessageCompensator(ReliableMessageSendService reliableMessageSendService) {
        this.reliableMessageSendService = reliableMessageSendService;
    }

    @Override
    public void compensate(Date startDate, int sizePreTask) {
        this.reliableMessageSendService.loadAndResend(startDate, sizePreTask);
    }
}
