package com.osc.lego.core.smart;
/**
 * @author osc 2023/11/14.
 * 
 *
 *
 */
public enum ActionType {
    COMMAND, QUERY
}
