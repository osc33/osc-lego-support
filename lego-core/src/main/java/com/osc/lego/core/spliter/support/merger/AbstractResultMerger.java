package com.osc.lego.core.spliter.support.merger;

import com.osc.lego.core.spliter.ResultMerger;

import org.apache.commons.collections.CollectionUtils;

import java.util.List;

/**
 * @author osc 2023/7/9.
 * 
 * 
 */
public abstract class AbstractResultMerger<R>
    implements ResultMerger<R> {

    @Override
    public final R merge(List<R> rs) {
        if (CollectionUtils.isEmpty(rs)){
            return defaultValue();
        }

        return doMerge(rs);
    }

    abstract R doMerge(List<R> rs);

    protected R defaultValue(){
        return null;
    }
}
