package com.osc.lego.core.wide;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

/**
 * @author osc 2023/10/30.
 * 
 * 
 */
@Data
@Setter(AccessLevel.PROTECTED)
public abstract class WideIndexContext<ITEM_TYPE extends Enum<ITEM_TYPE> & WideItemType<ITEM_TYPE>> {
    private ITEM_TYPE type;
}
