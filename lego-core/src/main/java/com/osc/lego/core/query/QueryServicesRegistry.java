package com.osc.lego.core.query;

import com.google.common.collect.Lists;

import lombok.Getter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

/**
 * @author osc 2023/10/13.
 * 
 *
 */
public class QueryServicesRegistry {
    @Autowired
    private ApplicationContext applicationContext;

    @Getter
    private List<Object> queryServices = Lists.newArrayList();

    @PostConstruct
    public void init(){
        Map<String, Object> beansWithAnnotation = applicationContext.getBeansWithAnnotation(QueryServiceDefinition.class);
        this.queryServices.addAll(beansWithAnnotation.values());
    }
}
