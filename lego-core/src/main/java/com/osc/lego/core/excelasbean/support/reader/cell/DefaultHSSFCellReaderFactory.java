package com.osc.lego.core.excelasbean.support.reader.cell;

import org.springframework.core.annotation.Order;

import java.lang.reflect.Field;


@Order(Integer.MAX_VALUE)
public class DefaultHSSFCellReaderFactory implements HSSFCellReaderFactory{

    @Override
    public HSSFCellReader createFor(String path, Field field) {
        return new DefaultHSSFCellReader();
    }

    @Override
    public boolean support(Field field) {
        return true;
    }
}
