package com.osc.lego.core.excelasbean.support.write.row;

import org.apache.poi.hssf.usermodel.HSSFRow;

/**
 *
 * 提供对 Row 的配置能力
 */
public interface HSSFRowConfigurator {
    void configFor(HSSFRowWriterContext context, HSSFRow row);
}
