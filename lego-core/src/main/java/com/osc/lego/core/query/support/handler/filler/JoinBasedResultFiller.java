package com.osc.lego.core.query.support.handler.filler;

import com.osc.lego.core.joininmemory.JoinService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;

@Order(Integer.MAX_VALUE)
public class JoinBasedResultFiller<R> implements SmartResultFiller<R>{
    @Autowired
    private JoinService joinService;

    @Override
    public R fill(R r) {
        if (r != null){
            joinService.joinInMemory(r);
        }
        return r;
    }

    @Override
    public boolean apply(Class resultClass) {
        return true;
    }

    @Override
    public String toString() {
        return "JoinInMemory";
    }
}
