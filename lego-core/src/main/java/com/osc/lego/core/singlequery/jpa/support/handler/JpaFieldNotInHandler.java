package com.osc.lego.core.singlequery.jpa.support.handler;

import com.osc.lego.annotation.singlequery.FieldNotIn;

import java.lang.reflect.Field;
import java.util.Collection;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 * @author osc 2023/8/31.
 *
 *
 */
public class JpaFieldNotInHandler
    extends AbstractJpaAnnotationHandler<FieldNotIn>{
    public JpaFieldNotInHandler() {
        super(FieldNotIn.class);
    }

    @Override
    public <E> Predicate create(Root<E> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder, FieldNotIn fieldNotIn, Object value) {
        if (value instanceof Collection){
            return criteriaBuilder.in(createExpression(root, fieldNameOf(fieldNotIn)))
                    .value((Collection<?>) value).not();
        }
        return null;
    }

    @Override
    protected boolean matchField(Field field, Class queryType) {
        return Collection.class.isAssignableFrom(queryType);
    }

    @Override
    protected String fieldNameOf(FieldNotIn fieldNotIn) {
        return fieldNotIn.value();
    }
}
