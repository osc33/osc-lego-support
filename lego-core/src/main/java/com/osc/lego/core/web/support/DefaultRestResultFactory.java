package com.osc.lego.core.web.support;

import com.osc.lego.core.validator.ValidateException;
import com.osc.lego.core.web.RestResult;
import com.osc.lego.core.web.RestResultFactory;

import javax.persistence.EntityNotFoundException;

/**
 * @author osc 2023/10/15.
 *
 *
 */
public class DefaultRestResultFactory implements RestResultFactory {
    @Override
    public <T> RestResult<T> success(T t) {
        return RestResult.success(t);
    }

    @Override
    public <T> RestResult<T> error(Throwable error) {
        String msg = getMsgFrom(error);
        return RestResult.error(msg);
    }

    private String getMsgFrom(Throwable error) {
        if (error instanceof ValidateException){
            return ((ValidateException)error).getMsg();
        }
        if (error instanceof EntityNotFoundException){
            return "数据丢失，请查证后重试";
        }
        return "ERROR";
    }
}
