package com.osc.lego.core.singlequery.jpa.support.handler;

import com.osc.lego.annotation.singlequery.FieldGreaterThanOrEqualTo;

import java.lang.reflect.Field;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 * @author osc 2023/8/31.
 *
 *
 */
public class JpaFieldGreaterThanOrEqualToHandler
    extends AbstractJpaAnnotationHandler<FieldGreaterThanOrEqualTo>{
    public JpaFieldGreaterThanOrEqualToHandler() {
        super(FieldGreaterThanOrEqualTo.class);
    }

    @Override
    public <E> Predicate create(Root<E> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder, FieldGreaterThanOrEqualTo fieldGreaterThanOrEqualTo, Object value) {
        if (value instanceof Comparable){
            return criteriaBuilder.greaterThanOrEqualTo(createExpression(root, fieldNameOf(fieldGreaterThanOrEqualTo)),
                    (Comparable) value);
        }
        return null;
    }

    @Override
    protected boolean matchField(Field field, Class queryType) {
        return Comparable.class.isAssignableFrom(queryType);
    }

    @Override
    protected String fieldNameOf(FieldGreaterThanOrEqualTo fieldGreaterThanOrEqualTo) {
        return fieldGreaterThanOrEqualTo.value();
    }
}
