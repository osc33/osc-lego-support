package com.osc.lego.core.singlequery;

/**
 * @author osc 2023/7/26.
 *
 * 
 *
 * 排序字段
 */
public interface OrderField {
    String getColumnName();
}
