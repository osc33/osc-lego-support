package com.osc.lego.core.excelasbean.support.write.row;

import com.osc.lego.core.excelasbean.support.write.column.HSSFColumnWriter;
import com.osc.lego.core.excelasbean.support.write.column.HSSFColumnWriterFactories;

import java.util.List;


public class DefaultHSSFRowWriterFactory implements HSSFRowWriterFactory{
    private final HSSFColumnWriterFactories columnWriterFactories;

    public DefaultHSSFRowWriterFactory(HSSFColumnWriterFactories columnWriterFactories) {
        this.columnWriterFactories = columnWriterFactories;
    }

    @Override
    public <D> HSSFRowWriter<D> create(Class<D> cls) {
        List<HSSFColumnWriter> writers = this.columnWriterFactories.createForCls(cls);
        return new DefaultHSSFRowWriter(writers);
    }

}
