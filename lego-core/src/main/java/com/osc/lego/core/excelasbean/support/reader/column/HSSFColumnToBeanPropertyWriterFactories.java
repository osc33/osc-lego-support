package com.osc.lego.core.excelasbean.support.reader.column;

import static java.util.stream.Collectors.toList;

import com.osc.lego.core.excelasbean.support.reader.parser.HSSFHeaderParser;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;

import org.apache.commons.collections.CollectionUtils;

import java.util.List;

/**
 *
 * 对 HSSFColumnToBeanWriterFactory 进行统一收口
 */
public class HSSFColumnToBeanPropertyWriterFactories {
    private final List<HSSFColumnToBeanPropertyWriterFactory> columnToBeanWriterFactories = Lists.newArrayList();
    private final HSSFHeaderParser headerParser;

    public HSSFColumnToBeanPropertyWriterFactories(List<HSSFColumnToBeanPropertyWriterFactory> columnToBeanWriterFactories,
                                                   HSSFHeaderParser headerParser) {
        Preconditions.checkArgument(CollectionUtils.isNotEmpty(columnToBeanWriterFactories));
        Preconditions.checkArgument(headerParser != null);

        this.headerParser = headerParser;

        if (CollectionUtils.isNotEmpty(columnToBeanWriterFactories)) {
            this.columnToBeanWriterFactories.addAll(columnToBeanWriterFactories);
        }

        // 添加 对 @HSSFEmbedded 的处理
        HSSFEmbeddedColumnToBeanPropertyWriterFactory embeddedColumnToBeanWriterFactory =
                new HSSFEmbeddedColumnToBeanPropertyWriterFactory(this, this.headerParser);
        this.columnToBeanWriterFactories.add(embeddedColumnToBeanWriterFactory);

    }


    public <D> List<HSSFColumnToBeanPropertyWriter> create(String parentPath, Class<D> cls) {
        // 从所有 Factory 中收集 HSSFColumnToBeanPropertyWriter
        return this.columnToBeanWriterFactories.stream()
                .flatMap(factory -> factory.create(parentPath, cls).stream())
                .collect(toList());
    }
}
