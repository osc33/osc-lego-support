package com.osc.lego.core.singlequery.jpa.support.handler;

import com.osc.lego.annotation.singlequery.FieldGreaterThan;

import java.lang.reflect.Field;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 * @author osc 2023/8/31.
 * 
 * 
 */
public class JpaFieldGreaterThanHandler
    extends AbstractJpaAnnotationHandler<FieldGreaterThan> {
    public JpaFieldGreaterThanHandler() {
        super(FieldGreaterThan.class);
    }

    @Override
    public <E> Predicate create(Root<E> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder, FieldGreaterThan fieldGreaterThan, Object value) {
        if (value instanceof Comparable) {
            return criteriaBuilder.greaterThan(createExpression(root, fieldNameOf(fieldGreaterThan)), (Comparable) value);
        }else {
            return null;
        }
    }

    @Override
    protected boolean matchField(Field field, Class queryType) {
        return Comparable.class.isAssignableFrom(queryType);
    }

    @Override
    protected String fieldNameOf(FieldGreaterThan fieldGreaterThan) {
        return fieldGreaterThan.value();
    }
}
