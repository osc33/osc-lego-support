package com.osc.lego.core.singlequery.jpa.support.handler;

import com.osc.lego.annotation.singlequery.FieldLessThanOrEqualTo;

import java.lang.reflect.Field;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 * @author osc 2023/8/31.
 * 
 * 
 */
public class JpaFieldLessThanOrEqualToHandler
    extends AbstractJpaAnnotationHandler<FieldLessThanOrEqualTo>{
    public JpaFieldLessThanOrEqualToHandler() {
        super(FieldLessThanOrEqualTo.class);
    }

    @Override
    public <E> Predicate create(Root<E> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder, FieldLessThanOrEqualTo fieldLessThanOrEqualTo, Object value) {
        if (value instanceof Comparable){
            return criteriaBuilder.lessThanOrEqualTo(createExpression(root, fieldNameOf(fieldLessThanOrEqualTo)), (Comparable) value);
        }
        return null;
    }

    @Override
    protected boolean matchField(Field field, Class queryType) {
        return Comparable.class.isAssignableFrom(queryType);
    }

    @Override
    protected String fieldNameOf(FieldLessThanOrEqualTo fieldLessThanOrEqualTo) {
        return fieldLessThanOrEqualTo.value();
    }
}
