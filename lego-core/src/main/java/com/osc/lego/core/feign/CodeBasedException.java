package com.osc.lego.core.feign;


public interface CodeBasedException {
    int getErrorCode();
    String getErrorMsg();
}
