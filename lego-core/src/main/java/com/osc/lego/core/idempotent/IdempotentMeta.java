package com.osc.lego.core.idempotent;

import com.osc.lego.annotation.idempotent.IdempotentHandleType;


public interface IdempotentMeta {
    String executorFactory();

    int group();

    String[] paramNames();

    String keyEl();

    IdempotentHandleType handleType();

    Class returnType();

}
