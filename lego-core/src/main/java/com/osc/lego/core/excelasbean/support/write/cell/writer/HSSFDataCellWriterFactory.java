package com.osc.lego.core.excelasbean.support.write.cell.writer;

import com.osc.lego.core.SmartComponent;

import java.lang.reflect.AnnotatedElement;


public interface HSSFDataCellWriterFactory extends SmartComponent<AnnotatedElement> {
    HSSFCellWriter create(AnnotatedElement element);
}
