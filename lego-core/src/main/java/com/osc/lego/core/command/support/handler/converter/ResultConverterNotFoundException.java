package com.osc.lego.core.command.support.handler.converter;

import lombok.Value;

@Value
public class ResultConverterNotFoundException extends RuntimeException{
    Class aggClass;
    Class contextClass;
    Class resultClass;
}
