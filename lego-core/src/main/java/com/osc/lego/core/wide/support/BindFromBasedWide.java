package com.osc.lego.core.wide.support;

import com.osc.lego.core.wide.Wide;
import com.osc.lego.core.wide.WideIndexCompareContext;
import com.osc.lego.core.wide.WideIndexSingleUpdateContext;
import com.osc.lego.core.wide.WideItemType;

/**
 * @author osc 2023/10/30.
 *
 * 
 */
public abstract class BindFromBasedWide<ID, ITEM_TYPE extends Enum<ITEM_TYPE> & WideItemType<ITEM_TYPE>>
        implements Wide<ID, ITEM_TYPE> {
    @Override
    public boolean isSameWithItem(WideIndexCompareContext<ITEM_TYPE> context) {
        return context.getWideWrapper().isSameWithItem(context.getItemData());
    }

    @Override
    public void updateByItem(WideIndexSingleUpdateContext<ITEM_TYPE> context) {
        context.getWideWrapper().updateItem(context.getItemData());
    }
}
