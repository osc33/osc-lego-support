package com.osc.lego.core.support.proxy;

/**
 * @author osc 2023/10/11.
 *
 *
 */
public interface ProxyObject {
    Class getInterface();
}
