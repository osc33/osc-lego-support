package com.osc.lego.core.excelasbean.support.reader.cell;

import org.apache.poi.hssf.usermodel.HSSFCell;


public interface HSSFCellReader {
    /**
     * 从 Cell 中读取数据
     * @param cell
     * @return
     */
    Object readValue(HSSFCell cell);
}
