package com.osc.lego.core.command;


public interface CommandForUpdateById<ID> extends CommandForUpdate {
    ID getId();
}
