package com.osc.lego.core.excelasbean.support.reader.sheet;

import com.osc.lego.core.excelasbean.support.reader.row.HSSFRowToBeanWriter;
import com.osc.lego.core.excelasbean.support.reader.row.HSSFRowToBeanWriterFactory;


public class DefaultHSSFSheetReaderFactory implements HSSFSheetReaderFactory {
    private final HSSFRowToBeanWriterFactory rowToBeanWriterFactory;

    public DefaultHSSFSheetReaderFactory(HSSFRowToBeanWriterFactory rowToBeanWriterFactory) {
        this.rowToBeanWriterFactory = rowToBeanWriterFactory;
    }

    @Override
    public <D> HSSFSheetReader<D> createFor(Class<D> cls) {
        HSSFRowToBeanWriter<D> rowToBeanWriter = rowToBeanWriterFactory.createForType(cls);
        return new DefaultHSSFSheetReader<>(cls, rowToBeanWriter);
    }
}
