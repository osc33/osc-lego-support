package com.osc.lego.core.spliter.support.spliter;


import com.osc.lego.common.splitter.SplittableParam;
import com.osc.lego.core.spliter.ParamSplitter;

import java.util.List;

/**
 * @author osc 2023/7/18.
 *
 *
 *
 * SplittableParam 拆分器
 *
 */
public class SplittableParamSplitter<P extends SplittableParam<P>>
        extends AbstractFixTypeParamSplitter<P>
        implements ParamSplitter<P> {

    @Override
    protected List<P> doSplit(P param, int maxSize) {
        return param.split(maxSize);
    }
}
