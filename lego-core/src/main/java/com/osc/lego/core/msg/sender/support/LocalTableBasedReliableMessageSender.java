package com.osc.lego.core.msg.sender.support;

import com.osc.lego.core.msg.sender.Message;
import com.osc.lego.core.msg.sender.ReliableMessageSender;

import lombok.extern.slf4j.Slf4j;

/**
 * @author osc 2023/10/16.
 *
 *
 */
@Slf4j
public class LocalTableBasedReliableMessageSender implements ReliableMessageSender {
    private final ReliableMessageSendService reliableMessageSendService;


    public LocalTableBasedReliableMessageSender(ReliableMessageSendService reliableMessageSendService) {
        this.reliableMessageSendService = reliableMessageSendService;
    }

    @Override
    public void send(Message message) {
        this.reliableMessageSendService.saveAndSend(message);
    }
}
