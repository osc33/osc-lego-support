package com.osc.lego.core.excelasbean.support.write.order;

import com.osc.lego.annotation.excelasbean.HSSFHeader;

import org.springframework.core.annotation.AnnotatedElementUtils;

import java.lang.reflect.AnnotatedElement;

/**
 * 从 HSSFIndex 中获取 index 信息
 */
public class HSSFHeaderBasedColumnOrderProvider implements HSSFColumnOrderProvider {
    @Override
    public boolean support(AnnotatedElement annotatedElement) {
        return AnnotatedElementUtils.hasAnnotation(annotatedElement, HSSFHeader.class);
    }

    @Override
    public int orderForColumn(AnnotatedElement annotatedElement) {
        return AnnotatedElementUtils.findMergedAnnotation(annotatedElement, HSSFHeader.class)
                .order();
    }
}
