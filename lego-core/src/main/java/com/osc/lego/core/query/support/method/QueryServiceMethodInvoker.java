package com.osc.lego.core.query.support.method;

import com.osc.lego.core.query.support.handler.QueryHandler;

import lombok.Builder;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.Method;

/**
 * @author osc 2023/9/29.
 * 
 * 
 */
@Value
@Builder
@Slf4j
public class QueryServiceMethodInvoker<Q, R>
        implements com.osc.lego.core.support.invoker.ServiceMethodInvoker {
    private final QueryHandler<R> queryHandler;

    @Override
    public final Object invoke(Method method, Object[] arguments) {
        return queryHandler.query(arguments);
    }

    @Override
    public String toString(){
        return this.queryHandler.toString();
    }
}
