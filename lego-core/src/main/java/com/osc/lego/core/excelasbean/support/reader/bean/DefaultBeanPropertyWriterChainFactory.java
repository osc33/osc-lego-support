package com.osc.lego.core.excelasbean.support.reader.bean;

import com.osc.lego.core.excelasbean.support.reader.cell.HSSFCellReader;
import com.osc.lego.core.excelasbean.support.reader.cell.HSSFCellReaderFactories;
import com.google.common.base.Preconditions;

import java.lang.reflect.Field;


public class DefaultBeanPropertyWriterChainFactory implements BeanPropertyWriterChainFactory{
    private final HSSFCellReaderFactories cellReaderFactories;
    private final BeanPropertyWriterFactories beanPropertyWriterFactories;

    public DefaultBeanPropertyWriterChainFactory(HSSFCellReaderFactories cellReaderFactories,
                                                 BeanPropertyWriterFactories beanPropertyWriterFactories) {
        Preconditions.checkArgument(cellReaderFactories != null);
        Preconditions.checkArgument(beanPropertyWriterFactories != null);

        this.cellReaderFactories = cellReaderFactories;
        this.beanPropertyWriterFactories = beanPropertyWriterFactories;
    }

    @Override
    public BeanPropertyWriterChain createForField(String path, Field field) {
        HSSFCellReader cellReader = this.cellReaderFactories.createFor(path, field);
        BeanPropertyWriter beanPropertyWriter = this.beanPropertyWriterFactories.createFor(path, field);
        return new BeanPropertyWriterChain(cellReader, beanPropertyWriter);
    }
}
