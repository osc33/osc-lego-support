package com.osc.lego.core.excelasbean.support.write.cell.configurator;

import com.osc.lego.annotation.excelasbean.HSSFHeader;

import org.springframework.core.annotation.AnnotatedElementUtils;

import java.lang.reflect.AnnotatedElement;

/**
 *
 * 为 Cell 提供 自动大小 支持
 *
 */
public class AutoSizeCellConfiguratorFactory implements HSSFHeaderCellConfiguratorFactory,
        HSSFDataCellConfiguratorFactory {
    @Override
    public boolean support(AnnotatedElement element) {
        return AnnotatedElementUtils.hasAnnotation(element, HSSFHeader.class);
    }

    @Override
    public HSSFCellConfigurator create(AnnotatedElement element) {
        HSSFHeader hssfHeader = AnnotatedElementUtils.findMergedAnnotation(element, HSSFHeader.class);
        return new AutoSizeCellConfigurator(hssfHeader.autoSizeColumn());
    }
}
