package com.osc.lego.core.excelasbean.support.write.cell.writer;

import lombok.Builder;
import lombok.Value;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;


@Value
@Builder
public class HSSFCellWriterContext {
    HSSFWorkbook workbook;
    HSSFSheet sheet;
    HSSFRow row;
}
