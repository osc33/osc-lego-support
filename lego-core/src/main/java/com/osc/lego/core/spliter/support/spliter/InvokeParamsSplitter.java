package com.osc.lego.core.spliter.support.spliter;


import com.osc.lego.core.spliter.ParamSplitter;

import java.util.List;

/**
 * @author osc 2023/7/6.
 * 
 * 
 *
 * 通用参数拆分器
 *
 */

public class InvokeParamsSplitter extends AbstractFixTypeParamSplitter<InvokeParams>{
    private final ParamSplitter paramSplitter;

    public InvokeParamsSplitter(ParamSplitter paramSplitter) {
        this.paramSplitter = paramSplitter;
    }

    @Override
    protected List<InvokeParams> doSplit(InvokeParams param, int maxSize) {
        return param.split(this.paramSplitter, maxSize);
    }
}
