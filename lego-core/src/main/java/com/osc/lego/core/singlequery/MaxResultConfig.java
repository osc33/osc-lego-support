package com.osc.lego.core.singlequery;

import com.osc.lego.annotation.singlequery.MaxResultCheckStrategy;

import lombok.Builder;
import lombok.Value;

/**
 * @author osc 2023/7/26.
 * 
 *
 *
 * 最大结果配置
 */
@Value
@Builder
public class MaxResultConfig {
    /**
     * 允许的最大结果数
     */
    private final int maxResult;

    /**
     * 结果检测策略
     */
    private final MaxResultCheckStrategy checkStrategy;
}
