package com.osc.lego.core.spliter.support;

import com.osc.lego.core.spliter.ParamSplitter;

/**
 * @author osc 2023/7/19.
 * 
 * 
 */
public interface ParamSplitterBuilder {
    boolean support(Class paramCls);

    ParamSplitter build(Class paramCls);
}
