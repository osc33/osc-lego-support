package com.osc.lego.core.excelasbean.support.write.cell;

import java.lang.reflect.AnnotatedElement;

public interface HSSFCellWriterChainFactory {
    /**
     * 创建 Header 写入链
     * @param element
     * @param <D>
     * @return
     */
    <D>HSSFCellWriterChain<D> createHeaderWriterChain(AnnotatedElement element);

    /**
     * 创建 Data 写入链
     * @param element
     * @param <D>
     * @return
     */
    <D>HSSFCellWriterChain<D> createDataWriterChain(AnnotatedElement element);
}
