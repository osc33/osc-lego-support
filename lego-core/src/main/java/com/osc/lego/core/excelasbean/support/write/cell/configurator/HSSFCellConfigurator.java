package com.osc.lego.core.excelasbean.support.write.cell.configurator;

import com.osc.lego.core.excelasbean.support.write.cell.writer.HSSFCellWriterContext;

import org.apache.poi.hssf.usermodel.HSSFCell;

/**
 *
 * 完成对 Cell 的配置
 */
public interface HSSFCellConfigurator {
    /**
     * 对 Cell 进行配置
     * @param context
     * @param columnIndex
     * @param cell
     */
    void configFor(HSSFCellWriterContext context, int columnIndex, HSSFCell cell);
}
