package com.osc.lego.core.validator;

import com.osc.lego.common.validator.ValidateErrorHandler;
import com.osc.lego.core.SmartComponent;

public interface BaseValidator<A> extends SmartComponent<A> {
    void validate(A a, ValidateErrorHandler validateErrorHandler);

    default void validate(A a){
        validate(a, ((name, code, msg) -> {
            throw new ValidateException(name, code, msg);
        }));
    }
}
