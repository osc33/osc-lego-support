package com.osc.lego.core.excelasbean.support.reader.column;

import com.osc.lego.core.excelasbean.support.reader.bean.BeanPropertyWriterChain;
import com.google.common.base.Preconditions;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;


@Data
public class HSSFColumnToBeanPropertyWriter {
    private final String path;
    private final String title;
    @Getter(AccessLevel.PRIVATE)
    private final BeanPropertyWriterChain propertyWriterChain;

    public HSSFColumnToBeanPropertyWriter(String path,
                                          String title,
                                          BeanPropertyWriterChain propertyWriterChain) {
        Preconditions.checkArgument(StringUtils.isNotEmpty(path));
        Preconditions.checkArgument(propertyWriterChain != null);
        this.path = path;
        this.title = title;
        this.propertyWriterChain = propertyWriterChain;
    }

    public void writeToBean(HSSFCell cell, Object bean){
        this.getPropertyWriterChain().writeToBean(cell, bean);
    }
}
