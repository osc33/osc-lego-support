package com.osc.lego.core.idempotent.support;

import com.osc.lego.core.idempotent.IdempotentExecutor;
import com.osc.lego.core.idempotent.IdempotentExecutorFactory;
import com.osc.lego.core.idempotent.IdempotentMeta;

import lombok.Setter;


@Setter
public class SimpleIdempotentExecutorFactory implements IdempotentExecutorFactory {
    private IdempotentKeyParser idempotentKeyParser;
    private ExecutionResultSerializer serializer;
    private ExecutionRecordRepository executionRecordRepository;

    @Override
    public IdempotentExecutor create(IdempotentMeta meta) {
        return new SimpleIdempotentExecutor(meta,
                idempotentKeyParser,
                this.serializer,
                this.executionRecordRepository);
    }
}
