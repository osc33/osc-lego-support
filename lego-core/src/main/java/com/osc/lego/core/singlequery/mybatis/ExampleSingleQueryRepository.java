package com.osc.lego.core.singlequery.mybatis;


public interface ExampleSingleQueryRepository<E, ID> extends com.osc.lego.core.singlequery.QueryObjectRepository<E>, com.osc.lego.core.singlequery.QueryIdRepository<E, ID> {
}
