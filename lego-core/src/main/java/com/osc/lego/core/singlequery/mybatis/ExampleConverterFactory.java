package com.osc.lego.core.singlequery.mybatis;

/**
 * @author osc 2023/8/26.
 * 
 *
 */
public interface ExampleConverterFactory {
    ExampleConverter createFor(Class example);
}
