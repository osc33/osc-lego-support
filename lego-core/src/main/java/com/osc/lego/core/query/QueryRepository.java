package com.osc.lego.core.query;

import com.osc.lego.core.singlequery.QueryObjectRepository;
import com.google.common.collect.Lists;

import org.apache.commons.collections4.CollectionUtils;

import java.util.Arrays;
import java.util.List;

/**
 * 通用查询接口
 * @param <E>
 * @param <ID>
 */
public interface QueryRepository<E, ID> extends QueryObjectRepository<E> {
    default E getById(ID id){
        if (id == null){
            return null;
        }
        List<E> data = getByIds(Arrays.asList(id));
        return CollectionUtils.isEmpty(data) ? null : data.get(0);
    }
    default List<E> getByIds(List<ID> ids){
        return Lists.newArrayList(findAllById(ids));
    }

    List<E> findAllById(Iterable<ID> ids);
}
