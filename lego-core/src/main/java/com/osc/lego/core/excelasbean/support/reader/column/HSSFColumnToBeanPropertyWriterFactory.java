package com.osc.lego.core.excelasbean.support.reader.column;

import java.util.List;


public interface HSSFColumnToBeanPropertyWriterFactory {
    /**
     * 创建 HSSFColumnToBeanPropertyWriter
     * @param parentPath 父path
     * @param cls
     * @param <D>
     * @return
     */
    <D> List<HSSFColumnToBeanPropertyWriter> create(String parentPath, Class<D> cls);
}
