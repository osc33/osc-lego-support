package com.osc.lego.core.excelasbean.support.reader.cell;

import com.osc.lego.core.SmartComponent;

import java.lang.reflect.Field;


public interface HSSFCellReaderFactory extends SmartComponent<Field> {
    HSSFCellReader createFor(String path, Field field);
}
