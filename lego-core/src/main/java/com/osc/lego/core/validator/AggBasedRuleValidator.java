package com.osc.lego.core.validator;

import com.osc.lego.common.validator.ValidateErrorHandler;
import com.osc.lego.core.command.AggRoot;

import org.springframework.stereotype.Component;

@Component
public class AggBasedRuleValidator<A> implements RuleValidator<A>{
    @Override
    public boolean support(A a) {
        return a instanceof AggRoot;
    }

    @Override
    public void validate(A a, ValidateErrorHandler validateErrorHandler) {
        ((AggRoot)a).validate();
    }
}
