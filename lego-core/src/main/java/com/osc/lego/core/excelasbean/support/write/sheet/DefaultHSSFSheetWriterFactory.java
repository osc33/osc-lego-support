package com.osc.lego.core.excelasbean.support.write.sheet;

import com.osc.lego.core.excelasbean.support.write.row.HSSFRowWriter;
import com.osc.lego.core.excelasbean.support.write.row.HSSFRowWriterFactory;
import com.google.common.base.Preconditions;


public class DefaultHSSFSheetWriterFactory implements HSSFSheetWriterFactory{
    private final HSSFRowWriterFactory rowWriterFactory;

    public DefaultHSSFSheetWriterFactory(HSSFRowWriterFactory rowWriterFactory) {
        Preconditions.checkArgument(rowWriterFactory != null);
        this.rowWriterFactory = rowWriterFactory;
    }


    @Override
    public <D> HSSFSheetWriter<D> createFor(Class<D> cls) {
        HSSFRowWriter hssfRowWriter = this.rowWriterFactory.create((Class) cls);
        return new DefaultHSSFSheetWriter(hssfRowWriter);
    }
}
