package com.osc.lego.core.query.support.handler.filler;

public interface ResultFiller<R> {
    R fill(R r);
}
