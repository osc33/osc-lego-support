package com.osc.lego.core.support.consumer;

/**
 * @author osc 2023/10/22.
 *
 *
 */
public interface ConsumerContainer {
    void start();

    void stop();
}
