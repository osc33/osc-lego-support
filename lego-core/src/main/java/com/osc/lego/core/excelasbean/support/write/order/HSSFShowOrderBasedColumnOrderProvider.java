package com.osc.lego.core.excelasbean.support.write.order;

import com.osc.lego.annotation.excelasbean.HSSFShowOrder;

import org.springframework.core.annotation.AnnotatedElementUtils;

import java.lang.reflect.AnnotatedElement;

/**
 * 从 HSSFShowOrder 中获取 index 信息
 */
public class HSSFShowOrderBasedColumnOrderProvider implements HSSFColumnOrderProvider {
    @Override
    public boolean support(AnnotatedElement annotatedElement) {
        return AnnotatedElementUtils.hasAnnotation(annotatedElement, HSSFShowOrder.class);
    }

    @Override
    public int orderForColumn(AnnotatedElement annotatedElement) {
        return AnnotatedElementUtils.findMergedAnnotation(annotatedElement, HSSFShowOrder.class)
                .value();
    }
}
