package com.osc.lego.core.excelasbean.support.write.cell.supplier;

import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang3.reflect.FieldUtils;

import java.lang.reflect.Field;


@Slf4j
public class FieldBasedDataValueSupplier implements HSSFValueSupplier {
    private final Field field;

    public FieldBasedDataValueSupplier(Field field) {
        this.field = field;
    }

    @Override
    public Object apply(Object o) {
        if (o == null){
            return null;
        }
        try {
            return FieldUtils.readField(field, o, true);
        } catch (IllegalAccessException e) {
            log.error("Failed to read field {} on {}", field, o, e);
        }
        return null;
    }
}
