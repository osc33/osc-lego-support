package com.osc.lego.core.command;

import lombok.Value;

/**
 * @author osc 2023/10/3.
 * 
 * 
 */
@Value
public class AggNotFoundException extends RuntimeException{
    Object id;

}
