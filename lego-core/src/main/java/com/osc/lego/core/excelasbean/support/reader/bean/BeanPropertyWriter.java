package com.osc.lego.core.excelasbean.support.reader.bean;


public interface BeanPropertyWriter {
    /**
     * 向 Bean 中写入数据
     * @param bean
     * @param value
     */
    public void writeToBean(Object bean, Object value);
}
