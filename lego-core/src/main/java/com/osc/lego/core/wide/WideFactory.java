package com.osc.lego.core.wide;

/**
 * @author osc 2023/10/27.
 *
 *
 */
public interface WideFactory<MASTER_ID, W extends Wide> {
    W create(MASTER_ID  masterId);
}
