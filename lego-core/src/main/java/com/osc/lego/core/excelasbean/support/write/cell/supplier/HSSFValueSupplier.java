package com.osc.lego.core.excelasbean.support.write.cell.supplier;

import java.util.function.Function;


@FunctionalInterface
public interface HSSFValueSupplier<T, R> extends Function<T, R> {
}
