package com.osc.lego.core.query;

import com.osc.lego.core.query.support.QueryConfiguration;
import com.osc.lego.core.query.support.QueryServiceBeanDefinitionRegistrar;

import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * @author osc 2023/9/25.
 *
 *
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@Import({QueryServiceBeanDefinitionRegistrar.class, QueryConfiguration.class})
public @interface EnableQueryService {
    /**
     * 扫描包
     * @return
     */
    String[] basePackages() default {};

    /**
     * 自定义实现Bean后缀
     * @return
     */
    String customImplementationPostfix() default "Impl";
}
