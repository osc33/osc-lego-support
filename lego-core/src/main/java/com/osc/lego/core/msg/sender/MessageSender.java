package com.osc.lego.core.msg.sender;

/**
 * @author osc 2023/10/16.
 *
 *
 */
public interface MessageSender {
    String send(Message message);
}
