package com.osc.lego.core.command.support.handler.contextfactory;

import lombok.Value;

@Value
public class ContextFactoryNotFoundException extends RuntimeException{
    Class cmdClass;
    Class contextClass;
}
