package com.osc.lego.core.singlequery.jpa;

import com.osc.lego.core.singlequery.QueryConverter;

import org.springframework.data.jpa.domain.Specification;

/**
 * @author osc 2023/8/30.
 *
 *
 */
public interface SpecificationConverter<E>
        extends QueryConverter<Specification<E>> {
}
