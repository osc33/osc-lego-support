package com.osc.lego.core.command;


public interface CommandForUpdateByKey<KEY> extends CommandForUpdate {
    KEY getKey();
}
