package com.osc.lego.core.excelasbean.support.write.sheet;

/**
 *
 * HSSFSheetWriter 工厂
 */
public interface HSSFSheetWriterFactory {
    /**
     * 创建 HSSFSheetWriter
     * @param cls
     * @param <D>
     * @return
     */
    <D> HSSFSheetWriter<D> createFor(Class<D> cls);
}
