package com.osc.lego.core.excelasbean.support.write.cell.configurator;

import com.osc.lego.core.SmartComponent;

import java.lang.reflect.AnnotatedElement;

/**
 *
 * 对 Data Cell 提供配置能力
 */
public interface HSSFDataCellConfiguratorFactory extends SmartComponent<AnnotatedElement> {
    HSSFCellConfigurator create(AnnotatedElement element);
}
