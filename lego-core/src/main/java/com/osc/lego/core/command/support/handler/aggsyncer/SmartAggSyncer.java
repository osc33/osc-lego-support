package com.osc.lego.core.command.support.handler.aggsyncer;

import com.osc.lego.core.command.AggRoot;

public interface SmartAggSyncer <AGG extends AggRoot> extends AggSyncer<AGG> {
    boolean apply(Class aggClass);
}
