package com.osc.lego.core.excelasbean.support.reader.bean;

import org.springframework.core.annotation.Order;

import java.lang.reflect.Field;


@Order(Integer.MAX_VALUE)
public class DefaultBeanPropertyWriterFactory implements BeanPropertyWriterFactory{
    @Override
    public BeanPropertyWriter createFor(String path, Field field) {
        return new DefaultBeanPropertyWriter(path);
    }

    @Override
    public boolean support(Field field) {
        return true;
    }
}
