package com.osc.lego.core.wide;

/**
 * @author osc 2023/10/28.
 * 
 * 
 */
public interface WideWrapperFactory<W extends Wide>   {
    WideWrapper<W> createForWide(W wide);
}
