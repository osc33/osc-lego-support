package com.osc.lego.core.excelasbean.support.write.cell.writer;

import org.springframework.core.annotation.Order;

import java.lang.reflect.AnnotatedElement;


@Order(Integer.MAX_VALUE)
public class DefaultHSSFCellWriterFactory implements HSSFHeaderCellWriterFactory, HSSFDataCellWriterFactory {
    @Override
    public HSSFCellWriter create(AnnotatedElement annotatedElement) {
        return new DefaultHSSFCellWriter();
    }

    @Override
    public boolean support(AnnotatedElement annotatedElement) {
        return true;
    }
}
