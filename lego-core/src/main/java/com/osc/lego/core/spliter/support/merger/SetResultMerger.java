package com.osc.lego.core.spliter.support.merger;

import com.osc.lego.core.spliter.SmartResultMerger;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author osc 2023/7/10.
 * 
 * 
 */
public class SetResultMerger
    extends AbstractFixTypeResultMerger<Set>
    implements SmartResultMerger<Set> {
    @Override
    Set doMerge(List<Set> sets) {
        return (Set) sets.stream()
                .flatMap(s -> s.stream())
                .collect(Collectors.toSet());
    }
}
