package com.osc.lego.core.command.support.handler.aggsyncer;

import lombok.Value;

@Value
public class AggSyncerNotFoundException extends RuntimeException{
    Class aggClass;
}
