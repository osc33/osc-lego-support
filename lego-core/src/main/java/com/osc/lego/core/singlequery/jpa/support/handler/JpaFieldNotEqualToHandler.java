package com.osc.lego.core.singlequery.jpa.support.handler;

import com.osc.lego.annotation.singlequery.FieldNotEqualTo;

import java.lang.reflect.Field;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 * @author osc 2023/8/31.
 *
 *
 */
public class JpaFieldNotEqualToHandler
    extends AbstractJpaAnnotationHandler<FieldNotEqualTo>{
    public JpaFieldNotEqualToHandler() {
        super(FieldNotEqualTo.class);
    }

    @Override
    public <E> Predicate create(Root<E> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder, FieldNotEqualTo fieldNotEqualTo, Object value) {
        return criteriaBuilder.notEqual(createExpression(root, fieldNameOf(fieldNotEqualTo)), value);
    }

    @Override
    protected boolean matchField(Field field, Class queryType) {
        return field.getType() == queryType;
    }

    @Override
    protected String fieldNameOf(FieldNotEqualTo fieldNotEqualTo) {
        return fieldNotEqualTo.value();
    }
}
