package com.osc.lego.core.excelasbean.support.write.sheet;

import org.apache.poi.hssf.usermodel.HSSFSheet;

import java.util.List;

/**
 *
 * 向 Sheet 中写入数据
 */
public interface HSSFSheetWriter<D> {
    /**
     * 写入 Header 和 Data
     * @param context
     * @param sheet
     * @param data
     */
    void writeHeaderAndData(HSSFSheetContext context, HSSFSheet sheet, List<D> data);

    /**
     * 仅写入 Header
     * @param context
     * @param sheet
     */
    void writeHeader(HSSFSheetContext context, HSSFSheet sheet);

    /**
     * 仅写入数据
     * @param context
     * @param sheet
     * @param data
     */
    void writeData(HSSFSheetContext context, HSSFSheet sheet, List<D> data);
}
