package com.osc.lego.core.query.support;

import com.osc.lego.core.query.QueryServiceDefinition;
import com.google.common.base.Preconditions;

import lombok.Value;

import org.springframework.core.annotation.AnnotatedElementUtils;

/**
 * @author osc 2023/9/25.
 * 
 * 
 */
@Value
public class QueryServiceMetadata {
    Class domainClass;

    Class repositoryClass;

    Class queryServiceClass;

    public static QueryServiceMetadata fromQueryService(Class queryService){
        Preconditions.checkArgument(queryService != null);

        QueryServiceDefinition definition = AnnotatedElementUtils.findMergedAnnotation(queryService, QueryServiceDefinition.class);
        if (definition == null){
            return null;
        }

        Class domainClass = definition.domainClass();
        Class repositoryClass = definition.repositoryClass();

        Preconditions.checkArgument(domainClass != null);
        Preconditions.checkArgument(repositoryClass != null);

        return new QueryServiceMetadata(domainClass, repositoryClass, queryService);
    }
}
