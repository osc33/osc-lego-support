package com.osc.lego.core.threadpool;

import lombok.Value;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang3.tuple.Pair;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.function.Function;

@Slf4j
public class GroupSubmitAndReturnService<T,R>
    extends AbstractGroupSubmitService<GroupSubmitAndReturnService.FutureAndTask<T,R>>{
    private final Function<List<T>, List<Pair<T, R>>> taskRunner;

    public GroupSubmitAndReturnService(String name,
                                       ExecutorService executorService,
                                       Function<List<T>, List<Pair<T, R>>> taskRunner){
        super(name, executorService);
        this.taskRunner = taskRunner;
    }


    public Future<R> submitTask(T task){
        CustomFuture customFuture = new CustomFuture<>();
        FutureAndTask futureAndTask = new FutureAndTask(customFuture, task);
        this.blockingQueue.add(futureAndTask);
        return futureAndTask.getFuture();
    }


    @Override
    protected Runnable buildTask(List<FutureAndTask<T, R>> tasks) {
        Map<T, List<FutureAndTask>> futureAndTaskListMap = new HashMap<>();

        tasks.stream().forEach(trFutureAndTask -> futureAndTaskListMap
                .computeIfAbsent(trFutureAndTask.task,(key)->new ArrayList<>()).add(trFutureAndTask));

        List<T> ts = new ArrayList<>(futureAndTaskListMap.keySet());
        return () ->{
            List<Pair<T, R>> results = this.taskRunner.apply(ts);
            for (Pair<T, R> result : results){
                List<FutureAndTask> futureAndTasks = futureAndTaskListMap.remove(result.getKey());
                futureAndTasks.stream().forEach(futureAndTask->futureAndTask.getFuture().setResult(result.getRight()));
            }
            //它遍历了 futureAndTaskListMap 中的每个值，而每个值都是一个任务列表，这些任务列表包含了在任务队列中但没有对应结果的任务。
            //然后，对于futureAndTaskListMap剩下的每个任务列表中的任务，它调用了 getFuture().setResult(null) 方法来将任务的结果设置为 null，表示该任务没有正常的执行结果。
            for (List<FutureAndTask> futureAndTasks : futureAndTaskListMap.values()){
                futureAndTasks.stream().forEach(futureAndTask->futureAndTask.getFuture().setResult(null));
            }
        };
    }


    @Value
    static class FutureAndTask<T, R>{
        CustomFuture<R> future;
        T task;
    }

    static class CustomFuture<R> extends FutureTask<R>{

        //super(() -> null) 中的 () -> null 表示 Callable<V> 函数式不为空，
        // 但是函数式的 V call() 方法执行后为空
        public CustomFuture() {
            super(() -> null);
        }

        public void setResult(R result) {
            set(result);
        }
    }

}
