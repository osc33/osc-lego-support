package com.osc.lego.core.excelasbean.support.reader.parser;

import com.osc.lego.annotation.excelasbean.HSSFTemplateHeader;

import org.apache.commons.lang3.StringUtils;

import java.lang.reflect.Field;


public class DefaultHSSFHeaderParser implements HSSFHeaderParser{

    @Override
    public String toTitle(String path, HSSFTemplateHeader hssfTemplateHeader) {
        StringBuilder value = new StringBuilder()
                .append(hssfTemplateHeader.title())
                .append("(").append(path).append(")");
        return value.toString();
    }

    @Override
    public String toPath(String parentPath, Field field) {
        return StringUtils.isEmpty(parentPath) ? field.getName() : parentPath + "." + field.getName();
    }

    @Override
    public String toPathFromTitle(String title) {
        int startIndex = title.indexOf('(');
        return title.substring(startIndex+1, title.length() -1);
    }
}
