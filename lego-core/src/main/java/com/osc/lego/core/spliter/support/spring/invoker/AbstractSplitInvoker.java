package com.osc.lego.core.spliter.support.spring.invoker;

import com.osc.lego.core.spliter.SplitService;

import lombok.Getter;

import java.lang.reflect.Method;

/**
 * @author osc 2023/7/6.
 *
 *
 *
 */
@Getter
abstract class AbstractSplitInvoker implements SplitInvoker {
    private final SplitService splitService;
    private final Method method;
    private final int sizePrePartition;

    protected AbstractSplitInvoker(SplitService splitService, Method method, int sizePrePartition) {
        this.splitService = splitService;
        this.method = method;
        this.sizePrePartition = sizePrePartition;
    }

}
