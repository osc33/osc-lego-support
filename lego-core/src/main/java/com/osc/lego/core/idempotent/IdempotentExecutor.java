package com.osc.lego.core.idempotent;

import org.aopalliance.intercept.MethodInvocation;


public interface IdempotentExecutor {
    Object invoke(MethodInvocation invocation) throws Throwable;
}
