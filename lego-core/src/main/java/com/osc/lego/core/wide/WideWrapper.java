package com.osc.lego.core.wide;

/**
 * @author osc 2023/10/26.
 *
 *
 */
public interface WideWrapper<W extends Wide> {
    W getTarget();

    default <I> void bindItem(I item){
        updateItem(item);
    }

    <I> boolean isSameWithItem(I item);

    <I> void updateItem(I item);
}
