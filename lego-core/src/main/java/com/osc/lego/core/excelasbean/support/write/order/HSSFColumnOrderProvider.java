package com.osc.lego.core.excelasbean.support.write.order;

import com.osc.lego.core.SmartComponent;

import java.lang.reflect.AnnotatedElement;


public interface HSSFColumnOrderProvider extends SmartComponent<AnnotatedElement> {
    boolean support(AnnotatedElement annotatedElement);

    int orderForColumn(AnnotatedElement annotatedElement);
}
