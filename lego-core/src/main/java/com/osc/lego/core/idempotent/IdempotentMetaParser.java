package com.osc.lego.core.idempotent;

import java.lang.reflect.Method;


public interface IdempotentMetaParser {
    IdempotentMeta parse(Method method);
}
