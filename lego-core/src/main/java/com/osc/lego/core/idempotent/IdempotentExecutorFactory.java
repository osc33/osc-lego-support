package com.osc.lego.core.idempotent;


public interface IdempotentExecutorFactory {
    IdempotentExecutor create(IdempotentMeta meta);
}
