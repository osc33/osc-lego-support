package com.osc.lego.core.idempotent.support;

import com.osc.lego.core.idempotent.IdempotentExecutor;
import com.osc.lego.core.idempotent.IdempotentExecutorFactory;
import com.osc.lego.core.idempotent.IdempotentMeta;
import com.google.common.collect.Maps;

import lombok.extern.slf4j.Slf4j;

import java.util.Map;


@Slf4j
public class IdempotentExecutorFactories {
    private final Map<String, IdempotentExecutorFactory> factoryMap = Maps.newHashMap();

    public IdempotentExecutorFactories(Map<String, IdempotentExecutorFactory> factoryMap){
        this.factoryMap.putAll(factoryMap);
    }

    public IdempotentExecutor create(IdempotentMeta meta) {
        if (meta == null){
            return NllIdempotentExecutor.getInstance();
        }

        IdempotentExecutorFactory idempotentExecutorFactory = factoryMap.get(meta.executorFactory());
        if (idempotentExecutorFactory == null){
            log.error("Failed to find IdempotentExecutorFactory for {}", meta.executorFactory());
            return NllIdempotentExecutor.getInstance();
        }
        return idempotentExecutorFactory.create(meta);
    }
}
