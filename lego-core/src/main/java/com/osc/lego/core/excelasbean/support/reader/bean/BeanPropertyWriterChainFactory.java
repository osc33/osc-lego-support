package com.osc.lego.core.excelasbean.support.reader.bean;

import java.lang.reflect.Field;


public interface BeanPropertyWriterChainFactory {
    /***
     * 为 Field 创建 WriterChain
     * @param path
     * @param field
     * @return
     */
    BeanPropertyWriterChain createForField(String path, Field field);
}
