package com.osc.lego.core.singlequery.jpa.support.handler;

import com.osc.lego.core.SmartComponent;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 * @author osc 2023/8/30.
 *
 *
 */
public interface JpaAnnotationHandler<A extends Annotation> extends SmartComponent<Annotation> {

    /**
     * 是否支持 Annotion
     * @param annotation
     * @return
     */
    @Override
    boolean support(Annotation annotation);

    /**
     * 创建 Predicate
     * @param root
     * @param query
     * @param criteriaBuilder
     * @param annotation
     * @param value
     * @param <E>
     * @return
     */
    <E> Predicate create(Root<E> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder, A annotation, Object value);

    /**
     * 查找 Entity 中的属性，主要用于配置检测
     * @param entityCls
     * @param a
     * @param queryType
     * @param <E>
     * @return
     */
    <E> Field findEntityField(Class<E> entityCls, A a, Class queryType);
}
