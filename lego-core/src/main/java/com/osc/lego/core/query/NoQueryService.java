package com.osc.lego.core.query;

import java.lang.annotation.*;

/**
 * @author osc 2023/9/29.
 * 
 * 
 *
 * 标记该 Bean 非 QueryService，不会生成 proxy 对象
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Documented
public @interface NoQueryService {
}
