package com.osc.lego.core.singlequery.mybatis;


import com.osc.lego.core.singlequery.QueryConverter;

/**
 * @author osc 2023/8/26.
 * 
 *
 *
 * 将查询对象转换为 Example 对象
 */
public interface ExampleConverter<E> extends QueryConverter<E> {

}
