package com.osc.lego.core.command.support;

import com.osc.lego.core.command.CommandServiceDefinition;
import com.google.common.base.Preconditions;

import lombok.Builder;
import lombok.Value;

import org.springframework.core.annotation.AnnotatedElementUtils;


@Value
@Builder
public class CommandServiceMetadata {
    private Class domainClass;

    private Class repositoryClass;

    private Class commandServiceClass;

    public static CommandServiceMetadata fromCommandService(Class commandService){
        CommandServiceDefinition mergedAnnotation = AnnotatedElementUtils.findMergedAnnotation(commandService, CommandServiceDefinition.class);

        Preconditions.checkArgument(mergedAnnotation != null);

        return CommandServiceMetadata.builder()
                .domainClass(mergedAnnotation.domainClass())
                .repositoryClass(mergedAnnotation.repositoryClass())
                .commandServiceClass(commandService)
                .build();
    }
}
