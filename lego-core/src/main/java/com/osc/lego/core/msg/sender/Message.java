package com.osc.lego.core.msg.sender;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author osc 2023/10/16.
 *
 *
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Message {
    private boolean orderly;

    private String topic;

    private String shardingKey;

    private String msgKey;

    private String tag;

    private String msg;
}
