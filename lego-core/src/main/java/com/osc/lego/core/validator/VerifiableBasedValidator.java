package com.osc.lego.core.validator;

import com.osc.lego.common.validator.ValidateErrorHandler;
import com.osc.lego.common.validator.Verifiable;

/**
 * @author osc 2023/9/10.
 * 
 * 
 */
public class VerifiableBasedValidator {

    public void validate(Object target, ValidateErrorHandler validateErrorHandler) {
        if (target instanceof Verifiable){
            ((Verifiable) target).validate(validateErrorHandler);
        }
    }
}
