package com.osc.lego.core.smart;

import java.util.Map;

/**
 * @author osc 2023/11/14.
 * 
 * 
 *
 */
public interface ExceptionMapProvider {
    Map<Class<? extends Throwable>, Boolean> get();
}
