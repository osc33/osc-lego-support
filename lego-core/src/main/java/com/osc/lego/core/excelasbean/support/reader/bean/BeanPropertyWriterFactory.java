package com.osc.lego.core.excelasbean.support.reader.bean;

import com.osc.lego.core.SmartComponent;

import java.lang.reflect.Field;


public interface BeanPropertyWriterFactory extends SmartComponent<Field> {

    BeanPropertyWriter createFor(String path, Field field);
}
