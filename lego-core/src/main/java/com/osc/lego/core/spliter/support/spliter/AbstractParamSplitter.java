package com.osc.lego.core.spliter.support.spliter;


import com.osc.lego.core.spliter.ParamSplitter;

import java.util.Collections;
import java.util.List;

/**
 * @author osc 2023/7/6.
 * 
 *
 *
 * 参数拆分器公共父类
 */
abstract class AbstractParamSplitter<P> implements ParamSplitter<P> {

    @Override
    public final List<P> split(P param, int maxSize) {
        if (param == null){
            return defaultValue();
        }
        return doSplit(param, maxSize);
    }

    protected abstract List<P> doSplit(P param, int maxSize);


    protected List<P> defaultValue() {
        return Collections.emptyList();
    }
}
