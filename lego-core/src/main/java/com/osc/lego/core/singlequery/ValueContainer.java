package com.osc.lego.core.singlequery;

import java.util.List;

public interface ValueContainer<E> {
    List<E> getValues();
}
