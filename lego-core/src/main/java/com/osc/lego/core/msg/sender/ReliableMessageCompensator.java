package com.osc.lego.core.msg.sender;

import java.util.Date;

/**
 * @author osc 2023/10/16.
 *
 * 
 */
public interface ReliableMessageCompensator {
    void compensate(Date startDate, int sizePreTask);
}
