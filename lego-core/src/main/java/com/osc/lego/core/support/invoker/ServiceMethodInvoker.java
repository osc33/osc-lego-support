package com.osc.lego.core.support.invoker;

import java.lang.reflect.Method;

/**
 * @author osc 2023/9/29.
 * 
 * 
 */
public interface ServiceMethodInvoker {
    Object invoke(Method method, Object[] arguments) ;
}
