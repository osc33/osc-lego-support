package com.osc.lego.core.excelasbean.support.write.cell.style;

import com.osc.lego.core.excelasbean.support.write.cell.writer.HSSFCellWriterContext;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;

/**
 *
 * Style 工厂，根据配置创建不同的 HSSFStyle
 */
public interface HSSFCellStyleFactory {

    /**
     * 创建 Style
     * @param context
     * @param name
     * @return
     */
    HSSFCellStyle createStyle(HSSFCellWriterContext context, String name);
}
