package com.osc.lego.core.singlequery;

/**
 * @author osc 2023/8/30.
 * 
 * 
 */
public interface QueryConverter<E> {
    E convertForQuery(Object query);

    E convertForCount(Object query);

    Pageable findPageable(Object query);

    Sort findSort(Object query);

    void validate(Class cls);
}
