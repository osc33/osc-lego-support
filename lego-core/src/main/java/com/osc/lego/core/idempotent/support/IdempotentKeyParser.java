package com.osc.lego.core.idempotent.support;


public interface IdempotentKeyParser {
    String parse(String[] names, Object[] param, String el);
}
