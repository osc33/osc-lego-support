package com.osc.lego.core.feign;

import lombok.Value;


@Value
public class RpcException extends RuntimeException{
    String methodKey;
    int httpStatus;
    String remoteAppName;
    int errorCode;
    String errorMsg;

    public RpcException(String methodKey, int httpStatus, String remoteAppName, RpcErrorResult errorResult){
        this.methodKey = methodKey;
        this.httpStatus = httpStatus;
        this.remoteAppName = remoteAppName;
        if (errorResult != null){
            this.errorCode = errorResult.getCode();
            this.errorMsg = errorResult.getMsg();
        }else {
            this.errorCode = -1;
            this.errorMsg = "未知错误";
        }
    }
}
