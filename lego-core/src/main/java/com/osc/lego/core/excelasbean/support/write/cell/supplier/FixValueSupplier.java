package com.osc.lego.core.excelasbean.support.write.cell.supplier;


public class FixValueSupplier implements HSSFValueSupplier {
    private final Object value;

    public FixValueSupplier(Object value) {
        this.value = value;
    }

    @Override
    public Object apply(Object o) {
        return value;
    }
}
