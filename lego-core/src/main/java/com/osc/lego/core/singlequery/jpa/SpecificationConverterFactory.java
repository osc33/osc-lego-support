package com.osc.lego.core.singlequery.jpa;

/**
 * @author osc 2023/8/30.
 * 
 *
 */
public interface SpecificationConverterFactory {
    <E> SpecificationConverter<E> createForEntity(Class<E> entityCls);
}
