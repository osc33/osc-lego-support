package com.osc.lego.core.msg.sender.support;

import java.util.Date;
import java.util.List;

/**
 * @author osc 2023/10/16.
 *
 * 
 */
public interface LocalMessageRepository {
    void save(LocalMessage message);

    void update(LocalMessage message);

    List<LocalMessage> loadNotSuccessByUpdateGt(Date latestUpdateTime, int size);
}