package com.osc.lego.core.web.command;

import com.osc.lego.core.command.CommandServicesRegistry;
import com.osc.lego.core.web.support.WebMethodRegistry;

import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * @author osc 2023/10/11.
 * 
 * 
 */
public class CommandMethodRegistry extends WebMethodRegistry {
    @Autowired
    private CommandServicesRegistry commandServicesRegistry;


    @Override
    protected List<Object> getServices() {
        return commandServicesRegistry.getCommandServices();
    }
}
