package com.osc.lego.core.singlequery.mybatis.support;

import com.osc.lego.core.singlequery.mybatis.ExampleConverter;
import com.osc.lego.core.singlequery.mybatis.ExampleConverterFactory;
import com.osc.lego.core.singlequery.mybatis.support.handler.FieldAnnotationHandler;
import com.google.common.base.Preconditions;

import java.util.List;

/**
 * @author osc 2023/8/26.
 * 
 * 
 */
public class DefaultExampleConverterFactory implements ExampleConverterFactory {
    private final List<FieldAnnotationHandler> fieldAnnotationHandlers;

    public DefaultExampleConverterFactory(List<FieldAnnotationHandler> fieldAnnotationHandlers) {
        Preconditions.checkArgument(fieldAnnotationHandlers != null);
        this.fieldAnnotationHandlers = fieldAnnotationHandlers;
    }

    @Override
    public ExampleConverter createFor(Class example) {
        return new ReflectBasedExampleConverter(example, this.fieldAnnotationHandlers);
    }
}
