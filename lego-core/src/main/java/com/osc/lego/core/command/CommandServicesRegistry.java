package com.osc.lego.core.command;

import com.google.common.collect.Lists;

import lombok.Getter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;


public class CommandServicesRegistry {
    @Autowired
    private ApplicationContext applicationContext;

    @Getter
    private List<Object> commandServices = Lists.newArrayList();

    @PostConstruct
    public void init(){
        Map<String, Object> beansWithAnnotation = applicationContext.getBeansWithAnnotation(CommandServiceDefinition.class);
        this.commandServices.addAll(beansWithAnnotation.values());
    }
}
