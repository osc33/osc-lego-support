package com.osc.lego.core.command;

import com.osc.lego.common.validator.ValidateErrorHandler;


public interface Command {
    default void validate(ValidateErrorHandler errorHandler){

    }
}
