package com.osc.lego.core.excelasbean.support.write.cell.configurator;

import com.osc.lego.core.SmartComponent;

import java.lang.reflect.AnnotatedElement;

/**
 *
 * 对 Header Cell 提供配置能力
 */
public interface HSSFHeaderCellConfiguratorFactory extends SmartComponent<AnnotatedElement> {

    HSSFCellConfigurator create(AnnotatedElement element);
}
