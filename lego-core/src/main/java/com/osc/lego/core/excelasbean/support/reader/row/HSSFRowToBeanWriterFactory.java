package com.osc.lego.core.excelasbean.support.reader.row;


public interface HSSFRowToBeanWriterFactory {
    /**
     * 创建 cls 的 HSSFRowToBeanWriter
     * @param cls
     * @param <D>
     * @return
     */
    <D> HSSFRowToBeanWriter<D> createForType(Class<D> cls);
}
