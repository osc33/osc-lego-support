package com.osc.lego.core.excelasbean.support.write.row;

/**
 *
 * 创建行写入器
 */
public interface HSSFRowWriterFactory {
    /**
     * 创建行写入器
     * @param cls
     * @param <D>
     * @return
     */
    <D> HSSFRowWriter<D> create(Class<D> cls);
}
