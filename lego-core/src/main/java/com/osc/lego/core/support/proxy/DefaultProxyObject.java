package com.osc.lego.core.support.proxy;

/**
 * @author osc 2023/10/11.
 * 
 * 
 */
public class DefaultProxyObject implements ProxyObject{
    private final Class itf;

    public DefaultProxyObject(Class itf) {
        this.itf = itf;
    }

    @Override
    public Class getInterface() {
        return this.itf;
    }
}
