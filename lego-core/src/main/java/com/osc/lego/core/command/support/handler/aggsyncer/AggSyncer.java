package com.osc.lego.core.command.support.handler.aggsyncer;


import com.osc.lego.core.command.AggRoot;

public interface AggSyncer<AGG extends AggRoot> {
    void sync(AGG agg);
}
