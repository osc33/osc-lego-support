package com.osc.lego.core.web;

/**
 * @author osc 2023/10/15.
 * 
 * 
 */
public interface RestResultFactory {
    <T> RestResult<T> success(T t);

    <T> RestResult<T> error(Throwable error);
}
