package com.osc.lego.core.excelasbean.support.reader.bean;

import org.springframework.expression.Expression;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;

public class DefaultBeanPropertyWriter implements BeanPropertyWriter {
    private final ExpressionParser parser = new SpelExpressionParser();
    private final Expression expression;

    public DefaultBeanPropertyWriter(String path) {
        this.expression = parser.parseExpression(path);
    }

    @Override
    public void writeToBean(Object bean, Object value) {
        this.expression.setValue(bean, value);
    }
}
