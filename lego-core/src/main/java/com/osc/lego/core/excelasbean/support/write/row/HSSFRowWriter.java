package com.osc.lego.core.excelasbean.support.write.row;

/**
 *
 * Row 写入器，写入一行数据
 */
public interface HSSFRowWriter<D> {
    /**
     * 写入 Header 行
     * @param context
     */
    void writeHeaderRow(HSSFRowWriterContext context);

    /**
     * 写入 数据 行
     * @param context
     * @param data
     */
     void writeDataRow(HSSFRowWriterContext context, D data);
}
