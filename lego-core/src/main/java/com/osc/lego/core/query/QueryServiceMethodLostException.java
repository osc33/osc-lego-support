package com.osc.lego.core.query;

import lombok.Data;
import lombok.ToString;

import java.lang.reflect.Method;
import java.util.Set;

/**
 * @author osc 2023/9/29.
 * 
 * 
 *
 * QueryServiceMethod 丢失异常；
 */
@Data
@ToString(callSuper = true)
public class QueryServiceMethodLostException extends RuntimeException{
    private Set<Method> lostMethods;
    public QueryServiceMethodLostException(Set<Method> lostMethods) {
        super();
        this.lostMethods = lostMethods;
    }

    public QueryServiceMethodLostException(String message) {
        super(message);
    }

    public QueryServiceMethodLostException(String message, Throwable cause) {
        super(message, cause);
    }

    public QueryServiceMethodLostException(Throwable cause) {
        super(cause);
    }

    protected QueryServiceMethodLostException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
