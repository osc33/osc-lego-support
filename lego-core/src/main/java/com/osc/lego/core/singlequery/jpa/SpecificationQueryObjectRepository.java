package com.osc.lego.core.singlequery.jpa;

import com.osc.lego.core.singlequery.QueryObjectRepository;

/**
 * @author osc 2023/8/30.
 * 
 * 
 */
public interface SpecificationQueryObjectRepository<E> extends QueryObjectRepository<E> {
}
