package com.osc.lego.core.validator;

import lombok.Value;

/**
 * @author osc 2023/9/11.
 * 
 *
 */
@Value
public class ValidateException extends RuntimeException{
    private final String name;
    private final String code;
    private final String msg;
}
