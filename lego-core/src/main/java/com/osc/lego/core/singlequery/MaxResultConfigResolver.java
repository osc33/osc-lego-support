package com.osc.lego.core.singlequery;

/**
 * @author osc 2023/7/26.
 * 
 * 
 *
 *
 */
public interface MaxResultConfigResolver {
    /**
     * 获取对象上最大结果配置
     * @param query
     * @return
     */
    MaxResultConfig maxResult(Object query);
}
