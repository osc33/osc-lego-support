package com.osc.lego.core.excelasbean.support.write.cell.configurator;

import com.osc.lego.core.excelasbean.support.write.cell.writer.HSSFCellWriterContext;

import org.apache.poi.hssf.usermodel.HSSFCell;


public class AutoSizeCellConfigurator implements HSSFCellConfigurator{
    private final boolean autoSizeColumn;

    public AutoSizeCellConfigurator(boolean autoSizeColumn) {
        this.autoSizeColumn = autoSizeColumn;
    }

    @Override
    public void configFor(HSSFCellWriterContext context, int columnIndex, HSSFCell cell) {
        if (autoSizeColumn) {
            context.getSheet().autoSizeColumn(columnIndex);
            context.getSheet().setColumnWidth(columnIndex,
                    context.getSheet().getColumnWidth(columnIndex) * 20 / 10);
        }
    }
}
