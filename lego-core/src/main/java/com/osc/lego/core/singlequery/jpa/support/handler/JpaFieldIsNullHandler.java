package com.osc.lego.core.singlequery.jpa.support.handler;

import com.osc.lego.annotation.singlequery.FieldIsNull;

import java.lang.reflect.Field;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 * @author osc 2023/8/31.
 *
 * 
 */
public class JpaFieldIsNullHandler
    extends AbstractJpaAnnotationHandler<FieldIsNull>{
    public JpaFieldIsNullHandler() {
        super(FieldIsNull.class);
    }

    @Override
    public <E> Predicate create(Root<E> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder, FieldIsNull fieldIsNull, Object value) {
        if (value instanceof Boolean){
            if ((Boolean) value){
                return criteriaBuilder.isNull(createExpression(root, fieldNameOf(fieldIsNull)));
            }else {
                return criteriaBuilder.isNotNull(createExpression(root, fieldNameOf(fieldIsNull)));
            }
        }
        return null;
    }

    @Override
    protected boolean matchField(Field field, Class queryType) {
        return true;
    }

    @Override
    protected String fieldNameOf(FieldIsNull fieldIsNull) {
        return fieldIsNull.value();
    }
}
