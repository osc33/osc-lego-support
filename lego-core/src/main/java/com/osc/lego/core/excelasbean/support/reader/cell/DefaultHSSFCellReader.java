package com.osc.lego.core.excelasbean.support.reader.cell;

import lombok.extern.slf4j.Slf4j;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DateUtil;


@Slf4j
public class DefaultHSSFCellReader implements HSSFCellReader {

    @Override
    public Object readValue(HSSFCell cell){
        if (cell == null){
            return null;
        }
        CellType cellTypeEnum = cell.getCellTypeEnum();
        switch (cellTypeEnum){
            case STRING:
                return cell.getStringCellValue();
            case NUMERIC:
                if(DateUtil.isCellDateFormatted(cell)){
                    return cell.getDateCellValue();
                }else {
                    return cell.getNumericCellValue();
                }
            case BOOLEAN:
                return cell.getBooleanCellValue();
            case BLANK:
                // 空值
                return null;
            case FORMULA:
                // 公式，暂不支持
                break;
            case ERROR:
                // 错误
                break;
            case _NONE:
                break;
        }
        return null;
    }
}
