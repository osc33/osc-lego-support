package com.osc.lego.core.loader;

public interface LazyLoadProxyFactory {
    <T> T createProxyFor(T t);
}
