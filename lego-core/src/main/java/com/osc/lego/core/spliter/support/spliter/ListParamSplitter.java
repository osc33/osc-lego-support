package com.osc.lego.core.spliter.support.spliter;

import com.osc.lego.core.spliter.SmartParamSplitter;
import com.google.common.collect.Lists;

import java.util.List;

/**
 * @author osc 2023/7/6.
 * 
 * 
 *
 * List 拆分器
 *
 */
public class ListParamSplitter
        extends AbstractFixTypeParamSplitter<List>
        implements SmartParamSplitter<List> {

    @Override
    protected List<List> doSplit(List param, int maxSize) {
        return Lists.partition(param, maxSize);
    }
}
