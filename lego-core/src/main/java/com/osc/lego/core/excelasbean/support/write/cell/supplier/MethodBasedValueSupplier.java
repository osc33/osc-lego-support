package com.osc.lego.core.excelasbean.support.write.cell.supplier;

import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang3.reflect.MethodUtils;

import java.lang.reflect.Method;


@Slf4j
public class MethodBasedValueSupplier implements HSSFValueSupplier {
    private final Method method;

    public MethodBasedValueSupplier(Method method) {
        this.method = method;
    }


    @Override
    public Object apply(Object o) {
        if (o == null){
            return null;
        }

        try {
            return MethodUtils.invokeMethod(o, true, method.getName());
        } catch (Exception e) {
            log.error("Failed to invoke method {} on {}", method, o);
        }
        return null;
    }
}
