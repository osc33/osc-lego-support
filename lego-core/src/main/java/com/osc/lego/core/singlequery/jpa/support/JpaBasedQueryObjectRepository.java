package com.osc.lego.core.singlequery.jpa.support;

import com.osc.lego.core.singlequery.Page;
import com.osc.lego.core.singlequery.jpa.SpecificationConverterFactory;
import com.osc.lego.core.singlequery.jpa.SpecificationQueryObjectRepository;

import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;

import java.util.List;

import javax.persistence.EntityManager;

/**
 * @author osc 2023/9/24.
 * 
 * 
 */
public class JpaBasedQueryObjectRepository<E, ID>
        extends SimpleJpaRepository<E, ID>
        implements SpecificationQueryObjectRepository<E> {

    private final BaseSpecificationQueryObjectRepository<E> specificationQueryObjectRepository;

    public JpaBasedQueryObjectRepository(JpaEntityInformation<E, ?> entityInformation,
                                         EntityManager entityManager,
                                         SpecificationConverterFactory specificationConverterFactory) {
        super(entityInformation, entityManager);
        this.specificationQueryObjectRepository =
                new BaseSpecificationQueryObjectRepository(this,
                        entityInformation.getJavaType(),
                        specificationConverterFactory);
    }

    public JpaBasedQueryObjectRepository(Class<E> domainClass, EntityManager em, SpecificationConverterFactory specificationConverterFactory) {
        super(domainClass, em);
        this.specificationQueryObjectRepository = new BaseSpecificationQueryObjectRepository(this,
                domainClass,
                specificationConverterFactory);
    }

    @Override
    public void checkForQueryObject(Class cls) {
        this.specificationQueryObjectRepository.checkForQueryObject(cls);
    }

    @Override
    public <Q> List<E> listOf(Q query) {
        return this.specificationQueryObjectRepository.listOf(query);
    }

    @Override
    public <Q> Long countOf(Q query) {
        return this.specificationQueryObjectRepository.countOf(query);
    }

    @Override
    public <Q> E get(Q query) {
        return this.specificationQueryObjectRepository.get(query);
    }

    @Override
    public <Q> Page<E> pageOf(Q query) {
        return this.specificationQueryObjectRepository.pageOf(query);
    }

    public void init(){
        this.specificationQueryObjectRepository.init();
    }
}
