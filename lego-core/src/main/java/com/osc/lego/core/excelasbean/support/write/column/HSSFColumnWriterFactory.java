package com.osc.lego.core.excelasbean.support.write.column;

import java.lang.reflect.Field;
import java.lang.reflect.Method;


public interface HSSFColumnWriterFactory {
    boolean support(Method method);

    <D> HSSFColumnWriter<D> createForGetter(Method getter);

    boolean support(Field field);
    <D> HSSFColumnWriter<D> createForField(Field field);
}
