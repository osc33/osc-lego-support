package com.osc.lego.core.web.query;

import com.osc.lego.core.query.QueryServicesRegistry;
import com.osc.lego.core.web.support.WebMethodRegistry;

import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * @author osc 2023/10/13.
 * 
 * 
 */
public class QueryMethodRegistry extends WebMethodRegistry {
    @Autowired
    private QueryServicesRegistry queryServicesRegistry;

    protected List<Object> getServices(){
        return this.queryServicesRegistry.getQueryServices();
    }
}
