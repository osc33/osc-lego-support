package com.osc.lego.core.idempotent.support;

import com.osc.lego.core.idempotent.IdempotentExecutor;

import org.aopalliance.intercept.MethodInvocation;


public class NllIdempotentExecutor implements IdempotentExecutor {
    private static final NllIdempotentExecutor INSTANCE = new NllIdempotentExecutor();

    @Override
    public Object invoke(MethodInvocation invocation) throws Throwable {
        return invocation.proceed();
    }

    public static NllIdempotentExecutor getInstance(){
        return INSTANCE;
    }
}
