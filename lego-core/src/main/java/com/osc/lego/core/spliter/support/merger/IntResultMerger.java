package com.osc.lego.core.spliter.support.merger;

import com.osc.lego.core.spliter.SmartResultMerger;

import org.apache.commons.collections.CollectionUtils;

import java.util.List;

/**
 * @author osc 2023/7/9.
 *
 *
 */
public class IntResultMerger
        extends AbstractResultMerger<Integer>
        implements SmartResultMerger<Integer> {

    @Override
    public boolean support(Class<Integer> resultType) {
        return Integer.class == resultType || Integer.TYPE == resultType;
    }

    @Override
    Integer doMerge(List<Integer> integers) {
        if (CollectionUtils.isEmpty(integers)){
            return 0;
        }
        return integers.stream()
                .mapToInt(Integer::intValue)
                .sum();
    }
}
