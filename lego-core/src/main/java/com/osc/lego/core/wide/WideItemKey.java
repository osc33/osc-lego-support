package com.osc.lego.core.wide;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author osc 2023/10/30.
 * 
 * 
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class WideItemKey<TYPE extends Enum<TYPE> & WideItemType<TYPE>, KEY> {
    private TYPE type;
    private KEY key;
}
