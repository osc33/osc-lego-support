package com.osc.lego.core.excelasbean.support.write.sheet;

import lombok.Builder;
import lombok.Value;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;


@Value
@Builder
public class HSSFSheetContext {
    private final HSSFWorkbook workbook;
}
