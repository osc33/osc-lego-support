package com.osc.lego.core.joininmemory;

import java.util.List;


public interface JoinItemsExecutor<DATA> {
    void execute(List<DATA> datas);
}
