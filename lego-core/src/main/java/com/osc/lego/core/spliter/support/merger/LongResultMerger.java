package com.osc.lego.core.spliter.support.merger;

import com.osc.lego.core.spliter.SmartResultMerger;

import org.apache.commons.collections.CollectionUtils;

import java.util.List;

/**
 * @author osc 2023/7/9.
 *
 *
 */
public class LongResultMerger
        extends AbstractResultMerger<Long>
        implements SmartResultMerger<Long> {
    @Override
    Long doMerge(List<Long> longs) {
        if (CollectionUtils.isEmpty(longs)){
            return 0L;
        }
        return longs.stream()
                .mapToLong(Long::longValue)
                .sum();
    }

    @Override
    public boolean support(Class<Long> resultType) {
        return Long.class == resultType || Long.TYPE == resultType;
    }
}
