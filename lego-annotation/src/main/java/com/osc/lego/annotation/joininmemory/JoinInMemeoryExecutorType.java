package com.osc.lego.annotation.joininmemory;


public enum JoinInMemeoryExecutorType {
    PARALLEL, // 并行执行
    SERIAL // 串行执行
}
