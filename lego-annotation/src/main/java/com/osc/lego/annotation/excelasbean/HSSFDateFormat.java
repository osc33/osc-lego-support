package com.osc.lego.annotation.excelasbean;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 主要用于对 Date 的格式化
 *
 * @author osc
 * @date 2024/02/01
 */
@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface HSSFDateFormat {
    /**
     * 日期格式化
     * @return
     */
    String value();
}
