package com.osc.lego.annotation.idempotent;

public enum IdempotentHandleType {
    ERROR, // 抛出异常
    RESULT; // 返回执行结果
}
