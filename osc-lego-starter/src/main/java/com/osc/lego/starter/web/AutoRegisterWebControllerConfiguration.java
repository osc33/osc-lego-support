package com.osc.lego.starter.web;

//import com.osc.lego.core.web.WebMethodHandlerMapping;

import com.osc.lego.core.web.support.DefaultRestResultFactory;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * @author osc 2023/10/6.
 * 
 * 
 */
@Configuration
@ConditionalOnWebApplication
@Import({CommandWebConfiguration.class, QueryWebConfiguration.class})
public class AutoRegisterWebControllerConfiguration {
    @Bean
    @ConditionalOnMissingBean
    public DefaultRestResultFactory defaultRestResultFactory(){
        return new DefaultRestResultFactory();
    }

}
