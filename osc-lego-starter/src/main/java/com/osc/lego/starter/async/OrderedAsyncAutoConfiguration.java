package com.osc.lego.starter.async;

import com.osc.lego.annotation.async.AsyncForOrderedBasedRocketMQ;
import com.osc.lego.core.async.order.OrderedAsyncConsumerContainerRegistry;
import com.osc.lego.core.async.order.OrderedAsyncInterceptor;

import org.apache.rocketmq.spring.autoconfigure.RocketMQAutoConfiguration;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.aop.PointcutAdvisor;
import org.springframework.aop.support.DefaultPointcutAdvisor;
import org.springframework.aop.support.annotation.AnnotationMatchingPointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.DefaultParameterNameDiscoverer;
import org.springframework.core.ParameterNameDiscoverer;
import org.springframework.core.env.Environment;

/**
 * @author osc 2023/9/2.
 * 
 * 
 */
@Configuration
@AutoConfigureAfter(RocketMQAutoConfiguration.class)
@ConditionalOnBean(RocketMQTemplate.class)
public class OrderedAsyncAutoConfiguration {
    @Autowired
    private Environment environment;

    @Autowired
    private RocketMQTemplate rocketMQTemplate;

    private final ParameterNameDiscoverer parameterNameDiscoverer = new DefaultParameterNameDiscoverer();


    @Bean
    public OrderedAsyncInterceptor orderedAsyncInterceptor(){
        return new OrderedAsyncInterceptor(this.environment, this.rocketMQTemplate, parameterNameDiscoverer);
    }

    @Bean
    public OrderedAsyncConsumerContainerRegistry orderedAsyncConsumerContainerRegistry(){
        return new OrderedAsyncConsumerContainerRegistry(this.environment);
    }
    @Bean
    public PointcutAdvisor orderedAsyncPointcutAdvisor(@Autowired OrderedAsyncInterceptor sendMessageInterceptor){
        return new DefaultPointcutAdvisor(new AnnotationMatchingPointcut(null, AsyncForOrderedBasedRocketMQ.class),
                sendMessageInterceptor);
    }
}
