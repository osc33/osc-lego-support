package com.osc.lego.starter.web;

import com.osc.lego.core.query.QueryServicesRegistry;
import com.osc.lego.core.web.query.QueryDispatcherController;
import com.osc.lego.core.web.query.QueryMethodRegistry;
import com.osc.lego.core.web.query.QueryServicesProvider;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class QueryWebConfiguration {
    @Bean
    public QueryDispatcherController queryDispatcherController(){
        return new QueryDispatcherController();
    }

    @Bean
    public QueryMethodRegistry queryMethodRegistry(){
        return new QueryMethodRegistry();
    }

    @Bean
    public QueryServicesProvider queryServicesProvider(){
        return new QueryServicesProvider();
    }

    @Bean
    @ConditionalOnMissingBean
    public QueryServicesRegistry queryServicesRegistry(){
        return new QueryServicesRegistry();
    }
}
