package com.osc.lego.lazyloader.utils;

import java.util.concurrent.TimeUnit;

public class TimeUtils {
    public static void sleepAsMS(long time) {
        try {
            TimeUnit.MILLISECONDS.sleep(time);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
