package com.osc.lego.lazyloader.core;

/**
 * 延迟加载代理工厂
 *
 * @author peter
 * @date 2023/05/24
 */
public interface LazyLoadProxyFactory {
    /**
     * 创建代理
     *
     * @param t t
     * @return {@link T}
     */
    <T> T createProxyFor(T t);
}
