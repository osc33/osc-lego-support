package com.osc.lego.lazyloader.core.support;

import static com.osc.lego.lazyloader.utils.BeanUtil.getPropertyName;
import static com.osc.lego.lazyloader.utils.BeanUtil.isGetter;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.springframework.aop.ProxyMethodInvocation;
import org.springframework.cglib.proxy.InvocationHandler;

import java.lang.reflect.Method;
import java.util.Map;

/**
 * 懒加载器拦截器
 *
 * <p>1，InvocationHandler 是 JDK 中的接口
 *
 * <p>2，在 CGLIB 中，代理对象不需要实现接口，而是直接继承被代理类，然后通过实现 MethodInterceptor 接口来实现方法调用的拦截和增强， 这个接口与
 * InvocationHandler 的作用类似。
 *
 * @author peter
 * @date 2023/05/24
 */
public class LazyLoaderInterceptor implements InvocationHandler, MethodInterceptor {
    private final Map<String, PropertyLazyLoader> lazyLoaderCache;
    private final Object target;

    public LazyLoaderInterceptor(Map<String, PropertyLazyLoader> lazyLoaderCache, Object target) {
        this.target = target;
        this.lazyLoaderCache = lazyLoaderCache;
    }

    @Override
    public Object invoke(MethodInvocation methodInvocation) throws Throwable {
        if (methodInvocation instanceof ProxyMethodInvocation) {
            ProxyMethodInvocation proxyMethodInvocation = (ProxyMethodInvocation) methodInvocation;
            return invoke(
                    proxyMethodInvocation.getProxy(),
                    proxyMethodInvocation.getMethod(),
                    proxyMethodInvocation.getArguments());
        }
        return invoke(
                methodInvocation.getThis(),
                methodInvocation.getMethod(),
                methodInvocation.getArguments());
    }

    @Override
    public Object invoke(Object o, Method method, Object[] objects) throws Throwable {
        if (isGetter(method)) {
            String propertyName = getPropertyName(method);
            PropertyLazyLoader propertyLazyLoader = this.lazyLoaderCache.get(propertyName);

            if (propertyLazyLoader != null) {
                Object data = method.invoke(target, objects);
                if (data != null) {
                    return data;
                }

                data = propertyLazyLoader.loadData(o);

                if (data != null) {
                    FieldUtils.writeField(target, propertyName, data, true);
                }
                return data;
            } else {
                // todo 解决 data = propertyLazyLoader.loadData(o);
                // 被拦截后的方法调用，即@LazyLoadBy("#{@userRepository.getById(cmd.userId)}") 的调用执行
                // 然后返回值给 data 返回
                method.invoke(target, objects);
            }
        }

        return method.invoke(target, objects);
    }
}
