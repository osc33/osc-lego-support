package com.osc.lego.lazyloader.config;

import com.osc.lego.lazyloader.core.LazyLoadProxyFactory;
import com.osc.lego.lazyloader.core.support.AutowiredLazyLoadProxyFactoryWrapper;
import com.osc.lego.lazyloader.core.support.DefaultLazyLoadProxyFactory;
import com.osc.lego.lazyloader.core.support.LazyLoaderInterceptorFactory;
import com.osc.lego.lazyloader.core.support.PropertyLazyLoaderFactory;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 延迟加载自动配置类--配置类
 *
 * @author peter
 * @date 2023/05/24
 */
@Configuration
public class LazyLoadAutoConfiguration {

    @Bean
    public LazyLoadProxyFactory lazyLoadProxyFactory(
            LazyLoaderInterceptorFactory lazyLoaderInterceptorFactory,
            ApplicationContext applicationContext) {
        LazyLoadProxyFactory lazyLoadProxyFactory =
                new DefaultLazyLoadProxyFactory(lazyLoaderInterceptorFactory);
        return new AutowiredLazyLoadProxyFactoryWrapper(lazyLoadProxyFactory, applicationContext);
    }

    @Bean
    public LazyLoaderInterceptorFactory lazyLoaderInterceptorFactory(
            PropertyLazyLoaderFactory lazyLoaderFactory) {
        return new LazyLoaderInterceptorFactory(lazyLoaderFactory);
    }

    @Bean
    public PropertyLazyLoaderFactory propertyLazyLoaderFactory(
            ApplicationContext applicationContext) {
        return new PropertyLazyLoaderFactory(applicationContext);
    }
}
