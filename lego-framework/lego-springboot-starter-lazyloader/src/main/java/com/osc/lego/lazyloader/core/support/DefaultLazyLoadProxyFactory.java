package com.osc.lego.lazyloader.core.support;

import com.osc.lego.lazyloader.core.LazyLoadProxyFactory;

import org.springframework.aop.framework.ProxyFactory;

/**
 * 默认延迟加载代理工厂
 *
 * @author peter
 * @date 2023/05/24
 */
public class DefaultLazyLoadProxyFactory extends AbstractLazyLoadProxyFactory
        implements LazyLoadProxyFactory {
    private final LazyLoaderInterceptorFactory lazyLoaderInterceptorFactory;

    public DefaultLazyLoadProxyFactory(LazyLoaderInterceptorFactory lazyLoaderInterceptorFactory) {
        this.lazyLoaderInterceptorFactory = lazyLoaderInterceptorFactory;
    }

    @Override
    protected <T> T createProxyFor(Class cls, T t) {
        ProxyFactory proxyFactory = new ProxyFactory();
        proxyFactory.setTarget(t);
        proxyFactory.addAdvice(this.lazyLoaderInterceptorFactory.createFor(cls, t));
        return (T) proxyFactory.getProxy();
    }
}
