package com.osc.lego.lazyloader.core.support;

import com.osc.lego.lazyloader.core.LazyLoadProxyFactory;

import org.springframework.context.ApplicationContext;

/**
 * autowired延迟加载能力的代理工厂包装
 *
 * @author peter
 * @date 2023/05/24
 */
public class AutowiredLazyLoadProxyFactoryWrapper implements LazyLoadProxyFactory {
    private final LazyLoadProxyFactory lazyLoadProxyFactory;
    private final ApplicationContext applicationContext;

    public AutowiredLazyLoadProxyFactoryWrapper(
            LazyLoadProxyFactory lazyLoadProxyFactory, ApplicationContext applicationContext) {
        this.lazyLoadProxyFactory = lazyLoadProxyFactory;
        this.applicationContext = applicationContext;
    }

    @Override
    public <T> T createProxyFor(T t) {
        if (t != null) {
            // todo 这个上下文对象 手动注入原型bean到ioc中后，何时销毁的哦？ 在此处是spel表达式不能注入依赖吧
            // 对象 t 并没有被加入到 Spring 容器中，它仍然是由开发人员手动创建的对象。
            // 但是，通过applicationContext.getAutowireCapableBeanFactory().autowireBean(t) 方法，
            // Spring 会自动装配 t 对象中的所有依赖项，这样就可以利用 Spring 的依赖注入功能来管理 t 对象的依赖项。
            applicationContext.getAutowireCapableBeanFactory().autowireBean(t);
        }
        // todo 而这个上下文对象的 代理对象 才会解析出需要 依赖的bean注入, 这个代理对象也是在上加入spring的 ioc 么？
        // 这个上下文代理对象 未托管给 spring。
        // 原型bean 对象的生命周期不由spring管理，
        // 所有当原型对象被方法栈引用结束后，会被GC自动回收掉。
        // 需要注意的是，原型对象中的资源要在销毁之前进行释放，避免资源泄漏。
        // 1. 使用 @PreDestroy
        // 建议代码如下：
        // @Component
        // @Scope("prototype")
        // public class MyPrototypeBean {
        //     @PreDestroy
        //     public void cleanUp() {
        //         // Clean up resources
        //     }
        //     // Class implementation
        // }

        // 2. 使用@CleanUp注解
        // 非Spring容器管理的Bean，可以考虑使用Lombok是一个第三方库@CleanUp注解来自动释放资源。
        return lazyLoadProxyFactory.createProxyFor(t);

        // 开发人员手动new出来的对象,可以通过以下的api 托管给spring 单例容器，示例代码：
        //         ApplicationContext context = new AnnotationConfigApplicationContext();
        //         MyObject myObject = new MyObject();
        //         context.getAutowireCapableBeanFactory().autowireBean(myObject);
        //         context.getAutowireCapableBeanFactory().registerSingleton("myObject", myObject);
    }
}
