package com.osc.lego.lazyloader.core.support;

import com.osc.lego.lazyloader.core.LazyLoadProxyFactory;
import org.apache.commons.lang3.ClassUtils;

import java.lang.reflect.Modifier;

/**
 * 抽象延迟加载代理工厂
 *
 * @author peter
 * @date 2023/05/24
 */
abstract class AbstractLazyLoadProxyFactory implements LazyLoadProxyFactory {
    @Override
    public <T> T createProxyFor(T t) {
        if (t == null) {
            return null;
        }
        Class cls = t.getClass();
        // 基础类型直接返回
        if (cls.isPrimitive() || ClassUtils.isPrimitiveWrapper(cls)){
            return t;
        }
        // 跳过 final 类
        if (Modifier.isFinal(cls.getModifiers())){
            return t;
        }
        return createProxyFor(cls, t);
    }

    protected abstract <T> T createProxyFor(Class cls, T t);
}
