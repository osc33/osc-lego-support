package com.osc.lego.rocketmq.core.reliablemsg.sender.support;

import com.osc.lego.rocketmq.core.reliablemsg.sender.Message;
import com.osc.lego.rocketmq.core.reliablemsg.sender.ReliableMessageSender;

import lombok.extern.slf4j.Slf4j;

/**
 * @author osc
 * @date 2023/09/11
 */
@Slf4j
public class LocalTableBasedReliableMessageSender implements ReliableMessageSender {
    private final ReliableMessageSendService reliableMessageSendService;

    public LocalTableBasedReliableMessageSender(
            ReliableMessageSendService reliableMessageSendService) {
        this.reliableMessageSendService = reliableMessageSendService;
    }

    @Override
    public void send(Message message) {
        this.reliableMessageSendService.saveAndSend(message);
    }
}
