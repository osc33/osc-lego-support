package com.osc.lego.rocketmq.core.support.consumer;

/**
 * 消费者容器
 *
 * @author osc
 * @date 2023/08/22
 */
public interface ConsumerContainer {
    void start();

    void stop();
}
