package com.osc.lego.rocketmq.core.reliablemsg.sender;

public interface ReliableMessageSender {
    void send(Message message);
}