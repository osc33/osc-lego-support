package com.osc.lego.rocketmq.core.reliablemsg.sender;

import java.util.Date;

public interface ReliableMessageCompensator {
    void compensate(Date startDate, int sizePreTask);
}
