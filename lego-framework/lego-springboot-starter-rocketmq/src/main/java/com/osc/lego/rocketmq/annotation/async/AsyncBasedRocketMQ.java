package com.osc.lego.rocketmq.annotation.async;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 基于异步RocketMQ
 *
 * @author osc
 * @date 2023/08/22
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface AsyncBasedRocketMQ {
    /**
     * MQ topic
     *
     * @return
     */
    String topic();

    /**
     * MQ tag
     *
     * @return
     */
    String tag() default "*";

    /**
     * 消费组
     *
     * @return
     */
    String consumerGroup();

    /**
     * nameServer 配置
     *
     * @return
     */
    String nameServer() default "${rocketmq.name-server:}";

    /**
     * 消费者运行的 profile，主要用于发送和消费分离的场景
     *
     * <p>1，不配置时，默认注册消费者
     *
     * <p>2，配置的值（例如："dev"）与spring.profiles.active环境活动属性值（例如："dev"）相同时，注册消费者
     *
     * <p>3, 其他情况则不会注册消费者
     *
     * @return
     */
    String consumerProfile() default "";
}
