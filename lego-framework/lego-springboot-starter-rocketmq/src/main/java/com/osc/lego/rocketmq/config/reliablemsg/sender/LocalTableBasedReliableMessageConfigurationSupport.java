package com.osc.lego.rocketmq.config.reliablemsg.sender;

import com.osc.lego.rocketmq.core.reliablemsg.sender.MessageSender;
import com.osc.lego.rocketmq.core.reliablemsg.sender.ReliableMessageCompensator;
import com.osc.lego.rocketmq.core.reliablemsg.sender.ReliableMessageSender;
import com.osc.lego.rocketmq.core.reliablemsg.sender.support.*;

import org.springframework.context.annotation.Bean;

import javax.sql.DataSource;

/**
 * @author osc
 * @date 2023/09/11
 */
public abstract class LocalTableBasedReliableMessageConfigurationSupport {

    @Bean
    public ReliableMessageSender reliableMessageSender(
            ReliableMessageSendService reliableMessageSendService) {
        return new LocalTableBasedReliableMessageSender(reliableMessageSendService);
    }

    @Bean
    public ReliableMessageCompensator reliableMessageCompensator(
            ReliableMessageSendService reliableMessageSendService) {
        return new LocalTableBasedReliableMessageCompensator(reliableMessageSendService);
    }

    @Bean
    public ReliableMessageSendService reliableMessageSendService(
            LocalMessageRepository localMessageRepository, MessageSender messageSender) {
        return new ReliableMessageSendService(localMessageRepository, messageSender);
    }

    @Bean
    public LocalMessageRepository localMessageRepository() {
        return new JdbcTemplateBasedLocalMessageRepository(dataSource(), messageTable());
    }

    @Bean
    public MessageSender messageSender() {
        return createMessageSend();
    }

    protected abstract DataSource dataSource();

    protected abstract String messageTable();

    protected abstract MessageSender createMessageSend();
}
