package com.osc.lego.rocketmq.core.reliablemsg.sender;


public interface MessageSender {
    String send(Message message);
}
