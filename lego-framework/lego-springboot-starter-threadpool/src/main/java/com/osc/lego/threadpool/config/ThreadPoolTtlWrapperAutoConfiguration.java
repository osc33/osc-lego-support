package com.osc.lego.threadpool.config;


import com.osc.lego.threadpool.core.ThreadPoolExecutorTtlWrapperProcessor;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.context.annotation.Bean;

@AutoConfiguration
public class ThreadPoolTtlWrapperAutoConfiguration {
    @Bean
    public ThreadPoolExecutorTtlWrapperProcessor threadPoolExecutorTtlWrapperProcessor(){
        return new ThreadPoolExecutorTtlWrapperProcessor();
    }
}
