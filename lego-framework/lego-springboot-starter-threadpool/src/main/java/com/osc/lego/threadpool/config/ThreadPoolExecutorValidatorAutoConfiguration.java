package com.osc.lego.threadpool.config;



import com.osc.lego.threadpool.core.ThreadPoolExecutorValidator;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.context.annotation.Bean;

@AutoConfiguration
public class ThreadPoolExecutorValidatorAutoConfiguration {
    @Bean
    public ThreadPoolExecutorValidator threadPoolExecutorValidator(){
        return new ThreadPoolExecutorValidator();
    }
}
