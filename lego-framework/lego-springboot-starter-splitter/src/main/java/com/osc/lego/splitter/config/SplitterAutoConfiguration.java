package com.osc.lego.splitter.config;

import com.osc.lego.splitter.annotation.Split;
import com.osc.lego.splitter.core.ParamSplitter;
import com.osc.lego.splitter.core.SmartParamSplitter;
import com.osc.lego.splitter.core.suport.executor.ParallelMethodExecutor;
import com.osc.lego.splitter.core.suport.merger.IntResultMerger;
import com.osc.lego.splitter.core.suport.merger.ListResultMerger;
import com.osc.lego.splitter.core.suport.merger.LongResultMerger;
import com.osc.lego.splitter.core.suport.merger.SetResultMerger;
import com.osc.lego.splitter.core.suport.spliter.AnnBasedParamSplitterBuilder;
import com.osc.lego.splitter.core.suport.spliter.ListParamSplitter;
import com.osc.lego.splitter.core.suport.spliter.SetParamSplitter;
import com.osc.lego.splitter.core.suport.spliter.SplittableParamSplitter;
import com.osc.lego.splitter.core.suport.spring.SplitInterceptor;
import com.osc.lego.splitter.core.suport.spring.SplitInvokerProcessor;
import com.osc.lego.splitter.core.suport.spring.invoker.SplitInvokerRegistry;

import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang3.concurrent.BasicThreadFactory;
import org.springframework.aop.PointcutAdvisor;
import org.springframework.aop.support.DefaultPointcutAdvisor;
import org.springframework.aop.support.annotation.AnnotationMatchingPointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * AutoConfiguration 自动配置主要完成：<br>
 * 1. PointcutAdvisor, 拦截器配置 2. BeanProcessor，处理器配置，Spring 启动时，对 @Split 注解进行处理 3.
 * SplitInvokerRegistry，InvokerRegistry 使用单例特性，共享注册信息 4. ParallelMethodExecutor，多线程执行器 5.
 * ParamSplitter，预制参数拆分器 6. ResultMerger，预制结果合并器
 *
 * @author osc
 * @date 2023/08/22
 */
@Configuration
@Slf4j
public class SplitterAutoConfiguration {

    @Bean
    public PointcutAdvisor pointcutAdvisor(@Autowired SplitInterceptor splitInterceptor) {
        return new DefaultPointcutAdvisor(
                new AnnotationMatchingPointcut(null, Split.class), splitInterceptor);
    }

    @Bean
    public SplitInvokerProcessor splitInvokerProcessor(SplitInvokerRegistry splitInvokerRegistry) {
        return new SplitInvokerProcessor(splitInvokerRegistry);
    }

    @Bean
    @ConditionalOnMissingBean
    public SplitInterceptor splitInterceptor(SplitInvokerRegistry splitInvokerRegistry) {
        return new SplitInterceptor(splitInvokerRegistry);
    }

    @Bean
    public SplitInvokerRegistry splitInvokerRegistry() {
        return new SplitInvokerRegistry();
    }

    @Bean
    @ConditionalOnMissingBean
    public ParallelMethodExecutor defaultSplitExecutor() {
        return new ParallelMethodExecutor(createExecutor(), 2);
    }

    private ExecutorService createExecutor() {
        int cpu = Runtime.getRuntime().availableProcessors();
        int nThreads = cpu * 100;
        BasicThreadFactory basicThreadFactory =
                new BasicThreadFactory.Builder()
                        .namingPattern("Default-Split-Executor-Thread-%d")
                        .daemon(true)
                        .uncaughtExceptionHandler(
                                (thread, throwable) ->
                                        log.error(
                                                "failed to run task on thread {}",
                                                thread,
                                                throwable))
                        .build();

        return new ThreadPoolExecutor(
                nThreads,
                nThreads,
                0L,
                TimeUnit.MILLISECONDS,
                new SynchronousQueue<>(),
                basicThreadFactory,
                new ThreadPoolExecutor.CallerRunsPolicy());
    }

    @Bean
    public ListResultMerger listResultMerger() {
        return new ListResultMerger();
    }

    @Bean
    public LongResultMerger longResultMerger() {
        return new LongResultMerger();
    }

    @Bean
    public IntResultMerger intResultMerger() {
        return new IntResultMerger();
    }

    @Bean
    public SetResultMerger setResultMerger() {
        return new SetResultMerger();
    }

    @Bean
    public ParamSplitter listParamSplitter() {
        return new ListParamSplitter();
    }

    @Bean
    public ParamSplitter setParamSplitter() {
        return new SetParamSplitter();
    }

    @Bean
    public AnnBasedParamSplitterBuilder annBasedParamSplitterBuilder(
            List<SmartParamSplitter> smartParamSplitters) {
        return new AnnBasedParamSplitterBuilder(smartParamSplitters);
    }

    @Bean
    public SplittableParamSplitter splittableParamSplitter() {
        return new SplittableParamSplitter();
    }
}
