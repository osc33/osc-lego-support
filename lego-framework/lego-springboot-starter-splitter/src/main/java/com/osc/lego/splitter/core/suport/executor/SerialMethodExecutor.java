package com.osc.lego.splitter.core.suport.executor;

import com.osc.lego.splitter.core.MethodExecutor;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * 顺序执行器，依次执行
 *
 * @author osc
 * @date 2023/08/22
 */
public class SerialMethodExecutor extends AbstractMethodExecutor implements MethodExecutor {

    @Override
    protected <R, P> List<R> doExecute(Function<P, R> function, List<P> ps) {
        return ps.stream().map(p -> function.apply(p)).collect(Collectors.toList());
    }
}
