package com.osc.lego.splitter.core.suport.merger;

import com.osc.lego.splitter.core.ResultMerger;

import org.apache.commons.collections.CollectionUtils;

import java.util.List;

/**
 * @author osc
 * @date 2023/08/22
 */
public abstract class AbstractResultMerger<R> implements ResultMerger<R> {

    @Override
    public final R merge(List<R> rs) {
        if (CollectionUtils.isEmpty(rs)) {
            return defaultValue();
        }

        return doMerge(rs);
    }

    abstract R doMerge(List<R> rs);

    protected R defaultValue() {
        return null;
    }
}
