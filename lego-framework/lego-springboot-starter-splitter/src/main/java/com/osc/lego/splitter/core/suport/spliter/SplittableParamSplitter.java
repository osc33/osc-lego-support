package com.osc.lego.splitter.core.suport.spliter;




import com.osc.lego.common.splitter.SplittableParam;
import com.osc.lego.splitter.core.ParamSplitter;

import java.util.List;

/**
 *
 SplittableParam 拆分器
 *
 * @author osc
 * @date 2023/08/22
 */


public class SplittableParamSplitter<P extends SplittableParam<P>>
        extends AbstractFixTypeParamSplitter<P>
        implements ParamSplitter<P> {

    @Override
    protected List<P> doSplit(P param, int maxSize) {
        return param.split(maxSize);
    }
}
