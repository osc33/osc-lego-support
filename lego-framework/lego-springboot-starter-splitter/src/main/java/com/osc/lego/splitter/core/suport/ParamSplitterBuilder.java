package com.osc.lego.splitter.core.suport;

import com.osc.lego.splitter.core.ParamSplitter;

/**
 * @author osc
 * @date 2023/08/22
 */
public interface ParamSplitterBuilder {
    boolean support(Class paramCls);

    ParamSplitter build(Class paramCls);
}
