package com.osc.lego.splitter.core.suport.spring.invoker;

import com.osc.lego.splitter.core.SplitService;

import lombok.Getter;

import java.lang.reflect.Method;

/**
 * 摘要将调用程序
 *
 * @author osc
 * @date 2023/08/22
 */
@Getter
abstract class AbstractSplitInvoker implements SplitInvoker {
    private final SplitService splitService;
    private final Method method;
    private final int sizePrePartition;

    protected AbstractSplitInvoker(SplitService splitService, Method method, int sizePrePartition) {
        this.splitService = splitService;
        this.method = method;
        this.sizePrePartition = sizePrePartition;
    }
}
