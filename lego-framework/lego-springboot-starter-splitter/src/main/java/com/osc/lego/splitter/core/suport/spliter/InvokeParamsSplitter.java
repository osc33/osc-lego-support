package com.osc.lego.splitter.core.suport.spliter;




import com.osc.lego.splitter.core.ParamSplitter;

import java.util.List;

/**
 *
 通用参数拆分器
 *
 * @author osc
 * @date 2023/08/22
 */


public class InvokeParamsSplitter extends AbstractFixTypeParamSplitter<InvokeParams>{
    private final ParamSplitter paramSplitter;

    public InvokeParamsSplitter(ParamSplitter paramSplitter) {
        this.paramSplitter = paramSplitter;
    }

    @Override
    protected List<InvokeParams> doSplit(InvokeParams param, int maxSize) {
        return param.split(this.paramSplitter, maxSize);
    }
}
