package com.osc.lego.splitter.core.suport.spring.invoker;

/**
 * 拆分执行器，主要是对方法调用的封装
 *
 * @author osc
 * @date 2023/08/22
 */
public interface SplitInvoker {
    Object invoke(Object target, Object[] args);
}
