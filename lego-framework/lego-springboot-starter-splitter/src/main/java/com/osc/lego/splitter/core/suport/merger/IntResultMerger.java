package com.osc.lego.splitter.core.suport.merger;

import com.osc.lego.splitter.core.SmartResultMerger;

import org.apache.commons.collections.CollectionUtils;

import java.util.List;

/**
 * @author osc
 * @date 2023/08/22
 */
public class IntResultMerger extends AbstractResultMerger<Integer>
        implements SmartResultMerger<Integer> {

    @Override
    public boolean support(Class<Integer> resultType) {
        return Integer.class == resultType || Integer.TYPE == resultType;
    }

    @Override
    Integer doMerge(List<Integer> integers) {
        if (CollectionUtils.isEmpty(integers)) {
            return 0;
        }
        return integers.stream().mapToInt(Integer::intValue).sum();
    }
}
