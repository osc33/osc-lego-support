package com.osc.lego.splitter.core.suport.spliter;

import com.google.common.collect.Lists;
import com.osc.lego.splitter.core.SmartParamSplitter;

import java.util.List;

/**
 * List 拆分器
 *
 * @author osc
 * @date 2023/08/22
 */
public class ListParamSplitter extends AbstractFixTypeParamSplitter<List>
        implements SmartParamSplitter<List> {

    @Override
    protected List<List> doSplit(List param, int maxSize) {
        return Lists.partition(param, maxSize);
    }
}
