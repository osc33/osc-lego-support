package com.osc.lego.splitter.core.suport.merger;

import com.google.common.reflect.TypeToken;
import com.osc.lego.splitter.core.SmartResultMerger;

/**
 * 从泛型中获取类型，用于进行 support
 *
 * @author osc
 * @date 2023/08/22
 */
abstract class AbstractFixTypeResultMerger<R> extends AbstractResultMerger<R>
        implements SmartResultMerger<R> {
    private final Class supportType;

    public AbstractFixTypeResultMerger() {
        TypeToken<R> typeToken = new TypeToken<R>(getClass()) {};
        this.supportType = (Class) typeToken.getRawType();
    }

    @Override
    public final boolean support(Class<R> resultType) {
        if (resultType == null) {
            return false;
        }
        return this.supportType.isAssignableFrom(resultType);
    }
}
