package com.osc.lego.splitter.core.suport.merger;

import com.osc.lego.splitter.core.SmartResultMerger;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author osc
 * @date 2023/08/22
 */
public class SetResultMerger extends AbstractFixTypeResultMerger<Set>
        implements SmartResultMerger<Set> {
    @Override
    Set doMerge(List<Set> sets) {
        return (Set) sets.stream().flatMap(s -> s.stream()).collect(Collectors.toSet());
    }
}
