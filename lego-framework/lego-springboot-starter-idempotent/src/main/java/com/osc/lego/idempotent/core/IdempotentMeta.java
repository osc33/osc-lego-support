package com.osc.lego.idempotent.core;

import com.osc.lego.idempotent.annotation.IdempotentHandleType;

/**
 * 幂等元
 *
 * @author osc
 * @date 2023/08/22
 */
public interface IdempotentMeta {
    String executorFactory();

    int group();

    String[] paramNames();

    String keyEl();

    IdempotentHandleType handleType();

    Class returnType();
}
