package com.osc.lego.idempotent.core.support;

import com.osc.lego.idempotent.core.IdempotentExecutor;

import org.aopalliance.intercept.MethodInvocation;

/**
 * nll幂等执行器
 *
 * @author osc
 * @date 2023/08/22
 */
public class NllIdempotentExecutor implements IdempotentExecutor {
    private static final NllIdempotentExecutor INSTANCE = new NllIdempotentExecutor();

    @Override
    public Object invoke(MethodInvocation invocation) throws Throwable {
        return invocation.proceed();
    }

    public static NllIdempotentExecutor getInstance() {
        return INSTANCE;
    }
}
