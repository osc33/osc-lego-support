package com.osc.lego.idempotent.core.support;

/**
 * 执行记录库
 *
 * @author osc
 * @date 2023/08/22
 */
public interface ExecutionRecordRepository {
    void update(ExecutionRecord executionRecord);

    ExecutionRecord getOrCreate(int type, String key);
}
