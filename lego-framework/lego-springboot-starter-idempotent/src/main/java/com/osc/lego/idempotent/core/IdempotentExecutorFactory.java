package com.osc.lego.idempotent.core;

/**
 * 幂等执行器工厂
 *
 * @author osc
 * @date 2023/08/22
 */


public interface IdempotentExecutorFactory {
    IdempotentExecutor create(IdempotentMeta meta);
}
