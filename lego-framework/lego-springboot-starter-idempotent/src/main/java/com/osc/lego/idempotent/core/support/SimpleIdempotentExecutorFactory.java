package com.osc.lego.idempotent.core.support;

import com.osc.lego.idempotent.core.IdempotentExecutor;
import com.osc.lego.idempotent.core.IdempotentExecutorFactory;
import com.osc.lego.idempotent.core.IdempotentMeta;

import lombok.Setter;

/**
 * 简单幂等执行器工厂
 *
 * @author osc
 * @date 2023/08/22
 */
@Setter
public class SimpleIdempotentExecutorFactory implements IdempotentExecutorFactory {
    private IdempotentKeyParser idempotentKeyParser;
    private ExecutionResultSerializer serializer;
    private ExecutionRecordRepository executionRecordRepository;

    @Override
    public IdempotentExecutor create(IdempotentMeta meta) {
        return new SimpleIdempotentExecutor(
                meta, idempotentKeyParser, this.serializer, this.executionRecordRepository);
    }
}
