package com.osc.lego.idempotent.annotation;

/**
 * 幂等处理类型
 *
 * @author osc
 * @date 2023/08/22
 */
public enum IdempotentHandleType {
    ERROR, // 抛出异常
    RESULT; // 返回执行结果
}
