package com.osc.lego.idempotent.core.support;

/**
 * 执行结果序列化器
 *
 * @author osc
 * @date 2023/08/22
 */
public interface ExecutionResultSerializer {
    <T> String serializeResult(T data);

    <T> T deserializeResult(String data, Class<T> cls);

    <T extends Exception> String serializeException(T data);

    <T extends Exception> T deserializeException(String data);
}
