package com.osc.lego.idempotent.core.support;

import com.google.common.collect.Maps;
import com.osc.lego.idempotent.core.IdempotentExecutor;
import com.osc.lego.idempotent.core.IdempotentExecutorFactory;
import com.osc.lego.idempotent.core.IdempotentMeta;

import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * 幂等执行器工厂
 *
 * @author osc
 * @date 2023/08/22
 */
@Slf4j
public class IdempotentExecutorFactories {
    private final Map<String, IdempotentExecutorFactory> factoryMap = Maps.newHashMap();

    public IdempotentExecutorFactories(Map<String, IdempotentExecutorFactory> factoryMap) {
        this.factoryMap.putAll(factoryMap);
    }

    public IdempotentExecutor create(IdempotentMeta meta) {
        if (meta == null) {
            return NllIdempotentExecutor.getInstance();
        }

        IdempotentExecutorFactory idempotentExecutorFactory =
                factoryMap.get(meta.executorFactory());
        if (idempotentExecutorFactory == null) {
            log.error("Failed to find IdempotentExecutorFactory for {}", meta.executorFactory());
            return NllIdempotentExecutor.getInstance();
        }
        return idempotentExecutorFactory.create(meta);
    }
}
