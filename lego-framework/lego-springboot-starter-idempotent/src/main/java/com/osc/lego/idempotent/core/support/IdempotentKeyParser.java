package com.osc.lego.idempotent.core.support;

/**
 * 幂等关键解析器
 *
 * @author osc
 * @date 2023/08/22
 */
public interface IdempotentKeyParser {
    String parse(String[] names, Object[] param, String el);
}
