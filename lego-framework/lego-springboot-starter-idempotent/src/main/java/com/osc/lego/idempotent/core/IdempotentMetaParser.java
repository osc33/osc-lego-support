package com.osc.lego.idempotent.core;

import java.lang.reflect.Method;

/**
 * 幂等元解析器
 *
 * @author osc
 * @date 2023/08/22
 */
public interface IdempotentMetaParser {
    IdempotentMeta parse(Method method);
}
