package com.osc.lego.validator.core.support;

import com.osc.lego.common.validator.ValidateErrorHandler;

import com.osc.lego.common.validator.ValidateErrors;
import lombok.Getter;

/**
 * 错误收集器
 *
 * @author osc
 * @date 2023/08/17
 */
public class ErrorsCollector implements ValidateErrorHandler {

    @Getter private ValidateErrors validateErrors;

    @Override
    public void handleError(String name, String code, String msg) {
        getOrCreateValidateErrors().addError(name, code, msg);
    }

    private ValidateErrors getOrCreateValidateErrors() {
        if (this.validateErrors == null) {
            this.validateErrors = new ValidateErrors();
        }
        return this.validateErrors;
    }
}
