package com.osc.lego.validator.core.support;


import com.osc.lego.common.validator.ValidateErrorHandler;
import com.osc.lego.common.validator.Verifiable;

/**
 * 基于validateable验证器
 *
 * @author osc
 * @date 2023/05/24
 */
public class VerifiableBasedValidator {

    public void validate(Object target, ValidateErrorHandler validateErrorHandler) {
        if (target instanceof Verifiable) {
            ((Verifiable) target).validate(validateErrorHandler);
        }
    }
}
