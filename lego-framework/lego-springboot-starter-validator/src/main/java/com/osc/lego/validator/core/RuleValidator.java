package com.osc.lego.validator.core;

/**
 * 规则验证器
 *
 * @author osc
 * @date 2023/08/17
 */
public interface RuleValidator<A> extends BaseValidator<A> {}
