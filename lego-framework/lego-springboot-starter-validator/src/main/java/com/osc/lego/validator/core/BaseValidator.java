package com.osc.lego.validator.core;

import com.osc.lego.common.validator.ValidateErrorHandler;
import com.osc.lego.validator.core.support.ValidateException;

/**
 * 基础验证器，校验bean的参数，bean需要继承 FixTypeBeanValidator<参数类型>
 *
 * @author osc
 * @date 2023/08/17
 */
public interface BaseValidator<A> extends SmartComponent<A> {

    /**
     * 验证
     *
     * @param a 一个
     * @param validateErrorHandler 验证错误处理程序
     */
    void validate(A a, ValidateErrorHandler validateErrorHandler);

    /**
     * 验证
     *
     * @param a 一个
     */
    default void validate(A a) {
        validate(
                a,
                ((name, code, msg) -> {
                    throw new ValidateException(name, code, msg);
                }));
    }
}
