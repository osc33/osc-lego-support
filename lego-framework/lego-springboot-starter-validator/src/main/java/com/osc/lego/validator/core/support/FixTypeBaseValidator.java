package com.osc.lego.validator.core.support;

import com.osc.lego.validator.core.BusinessValidator;

import java.lang.reflect.ParameterizedType;

/**
 * 固定类型bean验证器
 *
 * @author osc
 * @date 2023/05/24
 */
public abstract class FixTypeBaseValidator<A> implements BusinessValidator<A> {
    private final Class<A> type;

    /**
     * 固定类型基础验证器
     *
     * @return
     */
    protected FixTypeBaseValidator() {
        Class<A> type =
                (Class<A>)
                        ((ParameterizedType) getClass().getGenericSuperclass())
                                .getActualTypeArguments()[0];
        this.type = type;
    }

    /**
     * 固定类型基础验证器
     *
     * @param type 类型
     * @return
     */
    protected FixTypeBaseValidator(Class<A> type) {
        this.type = type;
    }

    @Override
    public final boolean support(Object a) {
        // clazz.isInstance(a) 与 instanceof 运算符的动态等价物
        return this.type.isInstance(a);
    }
}
