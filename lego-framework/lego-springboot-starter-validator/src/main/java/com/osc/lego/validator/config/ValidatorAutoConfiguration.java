package com.osc.lego.validator.config;

import com.osc.lego.common.validator.ValidateErrors;
import com.osc.lego.common.validator.ValidateErrorsHandler;
import com.osc.lego.common.validator.Verifiable;
import com.osc.lego.validator.core.support.ValidateService;
import com.osc.lego.validator.core.support.VerifiableBasedValidator;
import com.osc.lego.validator.core.support.VerifiableMethodValidationInterceptor;

import org.hibernate.validator.internal.engine.ConstraintViolationImpl;
import org.springframework.aop.PointcutAdvisor;
import org.springframework.aop.support.DefaultPointcutAdvisor;
import org.springframework.aop.support.annotation.AnnotationMatchingPointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.validation.ValidationAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.validation.annotation.Validated;
import org.springframework.validation.beanvalidation.MethodValidationPostProcessor;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.executable.ExecutableValidator;

@Configuration
@ConditionalOnClass({Verifiable.class, ExecutableValidator.class})
// 先装配spring的的检验配置，且先执行spring的参数校验，再自动装配当前配置
@AutoConfigureAfter(ValidationAutoConfiguration.class)
public class ValidatorAutoConfiguration {

    @Bean
    public VerifiableMethodValidationInterceptor validateableMethodValidationInterceptor() {
        return new VerifiableMethodValidationInterceptor(validateService());
    }

    @Bean
    @ConditionalOnMissingBean
    public VerifiableBasedValidator validateableBasedValidator() {
        return new VerifiableBasedValidator();
    }

    @Bean
    @ConditionalOnMissingBean
    public ValidateErrorsHandler validateErrorReporter() {
        return new ValidateErrorsHandler() {
            @Override
            public void handleErrors(ValidateErrors errors) {
                // 1. 获取校验所有的错误 ConstraintViolation
                Set<? extends ConstraintViolation<?>> collect =
                        errors.getErrors().stream()
                                .map(
                                        error ->
                                                ConstraintViolationImpl.forBeanValidation(
                                                        "",
                                                        null,
                                                        null,
                                                        error.getMsg(),
                                                        null,
                                                        null,
                                                        null,
                                                        null,
                                                        null,
                                                        null,
                                                        error.getCode()))
                                .collect(Collectors.toSet());
                // 2.抛出所有的错误
                throw new ConstraintViolationException(collect);
            }
        };
    }

    @Bean
    public PointcutAdvisor verifiableMethodValidationAdvisor(
            @Autowired VerifiableMethodValidationInterceptor interceptor) {
        // 1. 在接口类中加上 Validated 注解的类所有方法将被拦截，使用这个拦截器 ValidateableMethodValidationInterceptor 拦截处理
        DefaultPointcutAdvisor pointcutAdvisor =
                new DefaultPointcutAdvisor(
                        new AnnotationMatchingPointcut(Validated.class, null, true), interceptor);
        pointcutAdvisor.setOrder(Ordered.LOWEST_PRECEDENCE);
        return pointcutAdvisor;
    }

    @Autowired
    public void configMethodValidationOrder(
            List<MethodValidationPostProcessor> methodValidationPostProcessors) {
        // 1.Validated
        // 校验类型方法校验后置处理器，
        // 如果我们使用了多个MethodValidationPostProcessor实例，它们的执行顺序是不确定的。
        // 因此，如果我们希望校验逻辑先于其他切面逻辑执行，最好只使用一个MethodValidationPostProcessor实例，
        // 并设置setBeforeExistingAdvisors(true)。
        methodValidationPostProcessors.forEach(
                methodValidationPostProcessor ->
                        // 方法在执行切面逻辑之前，先校验参数
                        methodValidationPostProcessor.setBeforeExistingAdvisors(true));
    }

    @Bean
    @ConditionalOnMissingBean
    public ValidateService validateService() {
        // 当前 1.spring bean 的校验在这里开始注入
        // 这里实现的是spring bean 的参数校验服务类
        // 要校验的bean BaseValidator 接口；或者继承了抽象类 FixTypeBeanValidator<A>
        return new ValidateService();
    }
}
