package com.osc.lego.validator.core;

/**
 * 参数验证器
 *
 * @author osc
 * @date 2023/08/17
 */
public interface ParamValidator<A> extends BaseValidator<A> {}
