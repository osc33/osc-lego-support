package com.osc.lego.validator.core;

/**
 * 业务验证器
 *
 * @author osc
 * @date 2023/08/17
 */
public interface BusinessValidator<A> extends BaseValidator<A> {}
