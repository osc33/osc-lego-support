package com.osc.lego.validator.core.support;

import lombok.Value;

/**
 * 验证异常
 *
 * @author osc
 * @date 2023/08/17
 */
@Value
public class ValidateException extends RuntimeException {
    String name;
    String code;
    String msg;
}
