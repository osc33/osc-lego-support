package com.osc.lego.validator.core.support;

import com.google.common.base.Preconditions;

import com.google.common.collect.Lists;
import com.osc.lego.common.validator.ValidateErrorHandler;
import com.osc.lego.common.validator.ValidateErrorsHandler;
import lombok.Getter;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;

import java.util.List;

/**
 * validateable方法验证拦截器
 *
 * @author osc
 * @date 2023/05/24
 */
public class VerifiableMethodValidationInterceptor implements MethodInterceptor {

    private final ValidateService validateService;

    public VerifiableMethodValidationInterceptor(ValidateService validateService) {
        Preconditions.checkArgument(validateService != null);
        this.validateService = validateService;

    }

    @Override
    public Object invoke(MethodInvocation invocation) throws Throwable {
        // 反射实现增强，对参数校验的处理（在spring的参数校验执行之后，再执行这个方法）
        Object[] arguments = invocation.getArguments();
        if (arguments.length > 0) {
            List<Object> params = Lists.newArrayList();
            for (Object argument : arguments) {
                // 如果参数实现了 Verifiable 接口则，则执行相应的业务校验；
                // 如果参数没有实现，则没有执行任何代码，接着往后执行
                params.add(argument);
            }
            this.validateService.validateParam(params);
        }
        // 执行目标方法
        return invocation.proceed();
    }
}
