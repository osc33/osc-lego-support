package com.osc.lego.joininmemory.core;

import java.util.List;

/**
 * 执行数据 join 操作
 *
 * @author osc
 * @date 2023/08/17
 */
public interface JoinItemExecutor<DATA> {
    /**
     * 执行
     *
     * @param datas 数据
     */
    void execute(List<DATA> datas);

    /**
     * 运行级别
     *
     * @return int
     */
    default int runOnLevel() {
        return 0;
    }
}
