package com.osc.lego.joininmemory.core;

import java.util.List;

/**
 * 加入项执行者工厂
 *
 * @author osc
 * @date 2023/08/17
 */
public interface JoinItemExecutorFactory {
    <DATA> List<JoinItemExecutor<DATA>> createForType(Class<DATA> cls);
}
