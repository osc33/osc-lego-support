package com.osc.lego.joininmemory.core;

import java.util.List;

/**
 * 加入项目执行人
 *
 * @author osc
 * @date 2023/08/17
 */
public interface JoinItemsExecutor<DATA> {
    void execute(List<DATA> datas);
}
