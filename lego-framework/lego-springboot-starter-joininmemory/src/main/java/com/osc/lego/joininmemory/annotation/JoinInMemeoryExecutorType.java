package com.osc.lego.joininmemory.annotation;

/**
 * 加入存储器执行器类型
 *
 * @author osc
 * @date 2023/08/17
 */
public enum JoinInMemeoryExecutorType {
    /** 并行执行 */
    PARALLEL,
    /** 串行执行 */
    SERIAL
}
