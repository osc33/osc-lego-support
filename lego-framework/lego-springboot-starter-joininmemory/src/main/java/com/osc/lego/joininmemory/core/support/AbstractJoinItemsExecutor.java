package com.osc.lego.joininmemory.core.support;

import com.google.common.base.Preconditions;
import com.osc.lego.joininmemory.core.JoinItemExecutor;
import com.osc.lego.joininmemory.core.JoinItemsExecutor;

import lombok.AccessLevel;
import lombok.Getter;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * 多项内存执行器抽象类
 *
 * @author osc
 * @date 2023/08/17
 */
abstract class AbstractJoinItemsExecutor<DATA> implements JoinItemsExecutor<DATA> {
    @Getter(AccessLevel.PROTECTED)
    private final Class<DATA> dataCls;

    @Getter(AccessLevel.PROTECTED)
    private final List<JoinItemExecutor<DATA>> joinItemExecutors;

    public AbstractJoinItemsExecutor(
            Class<DATA> dataCls, List<JoinItemExecutor<DATA>> joinItemExecutors) {
        Preconditions.checkArgument(dataCls != null);
        Preconditions.checkArgument(joinItemExecutors != null);

        this.dataCls = dataCls;
        this.joinItemExecutors = joinItemExecutors;
        Collections.sort(
                this.joinItemExecutors, Comparator.comparingInt(JoinItemExecutor::runOnLevel));
    }
}
