package com.osc.lego.joininmemory.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 加入内存配置
 *
 * @author osc
 * @date 2023/08/17
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface JoinInMemoryConfig {
    /**
     * 执行器类型
     *
     * @return {@link JoinInMemeoryExecutorType }
     */
    JoinInMemeoryExecutorType executorType() default JoinInMemeoryExecutorType.SERIAL;

    /**
     * 执行器名字
     *
     * @return {@link String }
     */
    String executorName() default "defaultExecutor";
}
