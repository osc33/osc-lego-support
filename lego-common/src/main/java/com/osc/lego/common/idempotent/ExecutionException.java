package com.osc.lego.common.idempotent;

/**
 * 执行异常
 *
 * @author osc
 * @date 2023/08/17
 */
public class ExecutionException extends RuntimeException {}
