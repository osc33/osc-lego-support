package com.osc.lego.common.splitter;

import java.util.List;

/**
 * 可剥离参数
 *
 * @author osc
 * @date 2023/08/17
 */
public interface SplittableParam<P extends SplittableParam<P>> {
    List<P> split(int maxSize);
}
