package com.osc.lego.common.idempotent;

/**
 * 重复提交异常
 *
 * @author osc
 * @date 2023/08/17
 */
public class RepeatedSubmitException extends RuntimeException {}
