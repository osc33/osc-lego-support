package com.osc.lego.common.enums;

/**
 * 自我描述枚举
 *
 * @author osc
 * @date 2023/08/17
 */
public interface SelfDescribedEnum {
    default String getName() {
        return name();
    }

    String name();

    String getDescription();
}
