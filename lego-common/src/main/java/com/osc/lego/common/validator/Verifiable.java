package com.osc.lego.common.validator;

/**
 * 可验证
 *
 * @author osc
 * @date 2023/08/17
 */
public interface Verifiable {
    /**
     * 验证
     *
     * @param validateErrorHandler 验证错误处理程序
     */
    void validate(ValidateErrorHandler validateErrorHandler);
}
