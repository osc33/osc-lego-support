package com.osc.lego.common.enums;

/**
 * 基于代码枚举
 *
 * @author peter
 * @date 2023/08/17
 */
public interface CodeBasedEnum {
    /**
     * 获取代码
     *
     * @return int
     */
    int getCode();
}
