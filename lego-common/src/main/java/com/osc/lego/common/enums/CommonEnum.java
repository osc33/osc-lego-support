package com.osc.lego.common.enums;

/**
 * 常见枚举
 *
 * @author osc
 * @date 2023/08/17
 */
public interface CommonEnum extends CodeBasedEnum, SelfDescribedEnum {
    default boolean match(String value) {
        if (value == null) {
            return false;
        }
        return value.equals(String.valueOf(getCode())) || value.equals(getName());
    }
}
