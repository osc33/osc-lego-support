package com.osc.lego.common.validator;

/**
 * 验证错误处理程序
 *
 * @author osc
 * @date 2023/08/17
 */
public interface ValidateErrorHandler {
    void handleError(String name, String code, String msg);
}
