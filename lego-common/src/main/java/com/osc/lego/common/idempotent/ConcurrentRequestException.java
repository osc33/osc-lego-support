package com.osc.lego.common.idempotent;

/**
 * 并发请求异常
 *
 * @author osc
 * @date 2023/08/17
 */
public class ConcurrentRequestException extends RuntimeException {}
