package com.osc.lego.common.validator;

import lombok.Value;

import java.util.ArrayList;
import java.util.List;

/**
 * 验证错误
 *
 * @author osc
 * @date 2023/08/17
 */
@Value
public class ValidateErrors {
    List<Error> errors = new ArrayList<>();

    /**
     * 添加错误
     *
     * @param name 名字
     * @param code 代码
     * @param msg 味精
     */
    public void addError(String name, String code, String msg) {
        Error error = new Error(name, code, msg);
        this.errors.add(error);
    }

    /**
     * 有错误
     *
     * @return boolean
     */
    public boolean hasError() {
        return !this.errors.isEmpty();
    }

    /**
     * 错误
     *
     * @author osc
     * @date 2023/08/17
     */
    @Value
    public static class Error {
        String name;
        String code;
        String msg;
    }
}
