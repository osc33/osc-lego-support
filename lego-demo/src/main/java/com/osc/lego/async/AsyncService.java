package com.osc.lego.async;

import com.osc.lego.rocketmq.annotation.async.AsyncBasedRocketMQ;
import com.osc.lego.rocketmq.annotation.async.AsyncForOrderedBasedRocketMQ;

import lombok.Getter;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class AsyncService {
    @Getter private final List<CallData> callDatas = new ArrayList<>();

    /**
     * 每一个加了注解的方法都将会是一个生产方法（即发送mq消息的方法）
     *
     * <p>因为未配置consumerProfile 属性值，所以会默认注册成一个消费者（即监听MQ的方法）
     *
     * @param id
     * @param name
     * @param bean
     */
    @AsyncBasedRocketMQ(
            topic = "${async.test.normal.topic}",
            tag = "asyncTest1",
            consumerGroup = "${async.test.normal.group1}")
    public void asyncTest1(Long id, String name, AsyncInputBean bean) {
        log.info("receive data id {}, name {}, bean", id, name, bean);

        CallData callData = new CallData(id, name, bean);
        this.callDatas.add(callData);
    }

    /**
     * 每一个加了注解的方法都将会是一个生产方法（即发送mq消息的方法）
     *
     * <p>因为配置的consumerProfile = "test" 的值与spring.profiles.active环境活动属性值（例如："dev"）相同时，注册消费者
     *
     * @param id
     * @param name
     * @param bean
     */
    @AsyncBasedRocketMQ(
            topic = "${async.test.normal.topic}",
            tag = "asyncTest2",
            consumerGroup = "${async.test.normal.group2}",
            consumerProfile = "dev")
    public void asyncTest2(Long id, String name, AsyncInputBean bean) {
        log.info("receive data id {}, name {}, bean", id, name, bean);

        CallData callData = new CallData(id, name, bean);
        this.callDatas.add(callData);
    }

    /**
     * 每一个加了注解的方法都将会是一个生产方法（即发送mq消息的方法）
     *
     * <p>因为配置的consumerProfile = "test"
     * 的值与spring.profiles.active环境活动属性值（例如："dev"）不相同时，这方法不会注册一个消费者，只是一个发送者方法
     *
     * @param id
     * @param name
     * @param bean
     */
    @AsyncBasedRocketMQ(
            topic = "${async.test.normal.topic}",
            tag = "asyncTest3",
            consumerGroup = "${async.test.normal.group2}",
            consumerProfile = "test")
    public void asyncTest3(Long id, String name, AsyncInputBean bean) {
        log.info("receive data id {}, name {}, bean", id, name, bean);

        CallData callData = new CallData(id, name, bean);
        this.callDatas.add(callData);
    }

    @AsyncForOrderedBasedRocketMQ(
            topic = "${async.test.order.topic}",
            tag = "asyncTest1",
            shardingKey = "#id",
            consumerGroup = "${async.test.order.group1}")
    public void asyncTestForOrder1(Long id, String name, AsyncInputBean bean) {
        log.info("receive data id {}, name {}, bean {}", id, name, bean);

        CallData callData = new CallData(id, name, bean);
        this.callDatas.add(callData);
    }

    @AsyncForOrderedBasedRocketMQ(
            topic = "${async.test.order.topic}",
            tag = "asyncTest2",
            shardingKey = "#id",
            consumerGroup = "${async.test.order.group2}")
    public void asyncTestForOrder2(Long id, String name, AsyncInputBean bean) {
        log.info("receive data id {}, name {}, bean {}", id, name, bean);

        CallData callData = new CallData(id, name, bean);
        this.callDatas.add(callData);
    }

    @Value
    public static class CallData {
        Long id;
        String name;
        AsyncInputBean bean;
    }
}
