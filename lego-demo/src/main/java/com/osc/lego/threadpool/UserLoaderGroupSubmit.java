package com.osc.lego.threadpool;


import com.osc.lego.core.threadpool.GroupSubmitAndReturnService;
import com.osc.lego.joininmemory.support.User;
import com.osc.lego.joininmemory.support.UserRepository;
import lombok.SneakyThrows;

import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

public class UserLoaderGroupSubmit {
    @Autowired
    private UserRepository userRepository;

    private GroupSubmitAndReturnService<Long, User> groupSubmitAndReturnService;

    @SneakyThrows
    public User getById(Long id){
//        return userRepository.getById(id);
        return this.groupSubmitAndReturnService.submitTask(id)
                .get();
    }

    @PostConstruct
    public void init(){
        this.groupSubmitAndReturnService = new GroupSubmitAndReturnService<>(
                "",
                Executors.newFixedThreadPool(5),
                userIds -> {
                    List<User> users = this.userRepository.getByIds(userIds);
                    return users.stream()
                            .map(user -> Pair.of(user.getId(), user))
                            .collect(Collectors.toList());
                }
        );
    }



}
