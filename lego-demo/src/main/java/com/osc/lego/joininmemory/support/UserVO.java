package com.osc.lego.joininmemory.support;

import lombok.Builder;
import lombok.Data;


@Data
@Builder
public class UserVO {
    private Long id;
    private String name;

    public static UserVO apply(User user){
        if (user == null){
            return null;
        }
        return UserVO.builder()
                .id(user.getId())
                .name(user.getName())
                .build();
    }
}
