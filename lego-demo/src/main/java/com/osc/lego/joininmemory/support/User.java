package com.osc.lego.joininmemory.support;

import lombok.Builder;
import lombok.Data;


@Data
@Builder
public class User {
    private Long id;
    private String name;
}
