package com.osc.lego.joininmemory.support;

import static java.util.stream.Collectors.toList;

import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public class UserRepository {
    public List<User> getByIds(List<Long> ids){
        return ids.stream()
                .map(id-> User.builder()
                        .id(id)
                        .name("Name-" + id)
                        .build()
                ).collect(toList());
    }
}
